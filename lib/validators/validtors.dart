class Validators {
  static isValidEmail(String email) {
    final regularExpression = RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
    return regularExpression.hasMatch(email);
  }
  static isNoSpecialCharacter(String text) {
    final regularExpression = RegExp(r"^[a-zA-Z0-9_.-@]*$");
    return regularExpression.hasMatch(text);
  }

  static isValidUsername(String username) =>
      username.length >= 6 && isNoSpecialCharacter(username);

  static isValidPassword(String password) =>
      password.length >= 6 && isNoSpecialCharacter(password);

  static isMatchPasswordConfirm(String password, String confirmPassword) =>
      password == confirmPassword;
}
