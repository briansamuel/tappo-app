import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class StorageService {
  static StorageService _storageService;
  static SharedPreferences _prefs;
  StorageService();
  static Future<StorageService> getInstance() async {
    if (_storageService == null) {
      // keep local instance till it is fully initialized.
      var secureStorage = StorageService._();
      await secureStorage._init();
      _storageService = secureStorage;
    }
    return _storageService;
  }

  StorageService._();
  Future _init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  Future<Map<String, Object>> getDataStorage(key) async {
    if (!_prefs.containsKey(key)) {
      return null;
    }
    final extractedData =
        json.decode(_prefs.getString(key)) as Map<String, Object>;

    return extractedData;
  }

  Future<void> setDataStorage(key, params) async {
    try {
      final dataStorage = json.encode(params);
      _prefs.setString(key, dataStorage);
    } catch (e) {
      throw (e);
    }
  }
}
