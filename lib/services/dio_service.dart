import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import '../configs/api.dart';
import 'storage_service.dart';

class DioService {
  static Dio _dio = new Dio();
  static StorageService _storageService = StorageService();
  static String _token;

  DioService() {
    _dio.options.baseUrl = Api().baseUrl;
    getToken().then((value) {
      _dio.options.headers['content-Type'] = 'application/json';
    });
  }
  Future<Map<String, dynamic>> getData({@required url, params}) async {
    try {
      await getToken();
      final response = await _dio.get(url, queryParameters: params);
      if (response.statusCode != 200) {
        return null;
      }
      final reponseData = response.data;
      return reponseData;
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<Map<String, dynamic>> postData({@required url, params}) async {
    final response = await _dio.post(url, data: json.encode(params));
    if (response.statusCode != 200) {
      throw Exception(response.statusMessage);
    }
    final reponseData = response.data;
    return reponseData;
  }

  Future<Map<String, dynamic>> postFormdata({@required url, params}) async {
    final response = await _dio.post(url, data: params);
    if (response.statusCode != 200) {
      return null;
    }
    final reponseData = response.data;
    return reponseData;
  }

  Future<void> getToken() async {
    _storageService = await StorageService.getInstance();
    final extractedData = await _storageService.getDataStorage('userData');

    if (extractedData != null) {
      _token = extractedData['token'];

      _dio.options.headers["authorization"] = "Bearer $_token";
    }
  }
}
