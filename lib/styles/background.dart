import 'package:flutter/material.dart';

List<Map> listBackground = [
  {
    "colors": [
      Color(0xFF6944ff),
      Color(0xFFff2846),
    ]
  },
  {"color": Colors.red.shade200},
  {"color": Colors.red.shade400},
  {"color": Colors.red.shade800},
  {"color": Colors.green.shade200},
  {"color": Colors.green.shade400},
  {"color": Colors.green.shade800},
  {"color": Colors.blue.shade200},
  {"color": Colors.blue.shade400},
  {"color": Colors.blue.shade800},
  {"color": Colors.yellow.shade200},
  {"color": Colors.yellow.shade400},
  {"color": Colors.yellow.shade800},
  {"color": Colors.amberAccent.shade200},
  {"color": Colors.amberAccent.shade400},
  {"color": Colors.amberAccent.shade700},
  {"color": Colors.pink.shade200},
  {"color": Colors.pink.shade400},
  {"color": Colors.pink.shade800},
  {"color": Colors.purple.shade200},
  {"color": Colors.purple.shade400},
  {"color": Colors.purple.shade800},
  {"color": Colors.black},
  {"color": Colors.white},
  {"color": Colors.grey.shade200},
  {"color": Colors.grey.shade400},
  {"color": Colors.grey.shade800},
  {
    "colors": [
      Colors.yellow,
      Colors.red,
      Colors.indigo,
      Colors.teal,
    ]
  },
  {
    "colors": [
      Colors.purple,
      Colors.red,
    ]
  },
];
