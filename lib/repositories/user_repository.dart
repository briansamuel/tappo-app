import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tappo/configs/api.dart';
import 'package:tappo/models/social.dart';
import 'package:tappo/models/user.dart';

import 'package:tappo/services/dio_service.dart';
import 'package:sentry/sentry.dart';

class UserRepository {
  final DioService _dioService = DioService();
  final Api _api = Api();
  User _currentUser;

  User get currentUser => this._currentUser;
  //contructor
  UserRepository();

  // Function getUser from Auth Token
  Future<User> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    dynamic responseData;
    try {
      responseData = await _dioService.getData(url: _api.userUrl);

      if (responseData['error'] != null) {
        throw Exception(responseData['error']);
      }
      _currentUser = User.fromJson(responseData);
     
      final userInfo = json.encode({
        'id': _currentUser.id,
        'full_name': _currentUser.fullname,
        'description': _currentUser.description,
        'address': _currentUser.address,
        'avatar': _currentUser.avatarUrl,
        'qr_code': _currentUser.qrCode,
        'background': _currentUser.background,
        'list_social': Social.listToString(_currentUser.socials),
      });
      prefs.setString('userInfo', userInfo);
      if (_currentUser == null) {
        throw Exception('Không tìm được người dùng');
      }
      return _currentUser;
    } catch (exception, stackTrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
        hint: responseData,
      );
      await prefs.clear();
      throw (exception);
    }
  }

  Future<User> getUserStorage() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userInfo')) {
      return null;
    }
    final extractedUserData =
        json.decode(prefs.getString('userInfo')) as Map<String, Object>;
    // extractedUserData['list_social'] =
    //     json.decode(extractedUserData['list_social']);
    _currentUser = User.fromJson(extractedUserData);
    return _currentUser;
  }

  Future<bool> updateProfile(User user) async {
    try {
      String fileName = '';
      if (user.avatarUrl != null) {
        fileName = user.avatarUrl.split('/').last;
        MultipartFile fileAvatar =
            await MultipartFile.fromFile(user.avatarUrl, filename: fileName);
      }

      FormData formData = FormData.fromMap({
        'full_name': user.fullname ?? null,
        'description': user.description ?? null,
        'address': user.address ?? null,
        "avatar": user.avatarUrl != null
            ? await MultipartFile.fromFile(user.avatarUrl, filename: fileName)
            : null,
      });
      final responseData = await _dioService.postFormdata(
          url: _api.updateProfileUrl, params: formData);
      if (responseData['status'] == false) {
        return false;
      }
      final dataUpdate = responseData['data'] as Map<String, dynamic>;
      _currentUser = await getUserStorage();
      if (_currentUser != null && dataUpdate != null) {
        dataUpdate.forEach((key, value) {
          _currentUser = _currentUser.copyWith(
            id: _currentUser.id,
            fullname: dataUpdate['full_name'] ?? null,
            address: dataUpdate['address'] ?? null,
            description: dataUpdate['description'] ?? null,
            avatarUrl: dataUpdate['avatar'] ?? null,
            qrCode: dataUpdate['qr_code'] ?? null,
          );
        });
      }
      final prefs = await SharedPreferences.getInstance();
      final userInfo = json.encode({
        'id': _currentUser.id,
        'full_name': _currentUser.fullname,
        'description': _currentUser.description,
        'address': _currentUser.address,
        'avatar': _currentUser.avatarUrl,
        'qr_code': _currentUser.qrCode,
        'background': _currentUser.background,
        'list_social': Social.listToString(_currentUser.socials),
      });
      prefs.setString('userInfo', userInfo);
      return true;
    } catch (exception, stackTrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );

      return false;
    }
  }

  Future<bool> updateSocial(List<Social> socials) async {
    try {
      FormData formData = FormData.fromMap({
        'list_social': socials != null ? Social.listToString(socials) : null,
      });
      final responseData = await _dioService.postFormdata(
          url: _api.updateProfileUrl, params: formData);
      if (responseData['status'] == false) {
        return false;
      }
      final dataUpdate = responseData['data'] as Map<String, dynamic>;
      _currentUser = await getUserStorage();
      if (_currentUser != null && dataUpdate != null) {
        dataUpdate.forEach((key, value) {
          _currentUser = _currentUser.copyWith(
              socials: Social.listFromJson(dataUpdate['list_social']));
        });
      }
      final prefs = await SharedPreferences.getInstance();
      final userInfo = json.encode({
        'id': _currentUser.id,
        'full_name': _currentUser.fullname,
        'description': _currentUser.description,
        'address': _currentUser.address,
        'avatar': _currentUser.avatarUrl,
        'qr_code': _currentUser.qrCode,
        'background': _currentUser.background,
        'list_social': Social.listToString(_currentUser.socials),
      });
      prefs.setString('userInfo', userInfo);
      return true;
    } catch (exception, stackTrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );

      return false;
    }
  }

  Future<bool> updateBackground(int background) async {
    try {
      FormData formData = FormData.fromMap({
        'background': background ?? 0,
      });
      final responseData = await _dioService.postFormdata(
          url: _api.changeBackground, params: formData);
      if (responseData['status'] == false) {
        return false;
      }

      _currentUser = await getUserStorage();
      if (_currentUser != null) {
        _currentUser = _currentUser.copyWith(background: background);
      }
      final prefs = await SharedPreferences.getInstance();
      final userInfo = json.encode({
        'id': _currentUser.id,
        'full_name': _currentUser.fullname,
        'description': _currentUser.description,
        'address': _currentUser.address,
        'avatar': _currentUser.avatarUrl,
        'qr_code': _currentUser.qrCode,
        'background': _currentUser.background,
        'list_social': Social.listToString(_currentUser.socials),
      });
      prefs.setString('userInfo', userInfo);
      return true;
    } catch (exception, stackTrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );

      return false;
    }
  }

  Future<bool> updatePassword(Map<String, String> dataPass) async {
    try {
      FormData formData = FormData.fromMap({
        'password': dataPass['oldPassword'] ?? null,
        'new_password': dataPass['newPassword'] ?? null,
        'confirm_new_password': dataPass['newPasswordComfirm'] ?? null,
      });
      final responseData = await _dioService.postFormdata(
          url: _api.changePassword, params: formData);
      if (responseData['status'] == false) {
        throw Exception(responseData['message']);
      }

      return true;
    } catch (exception, stackTrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stackTrace,
      );
      throw (exception);
    }
  }

}
