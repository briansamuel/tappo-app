import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/services/dio_service.dart';
import '../configs/api.dart';
import 'package:sentry/sentry.dart';

class AuthRepository {
  DioService _dioService = DioService();
  Api _api = Api();
  String _token;
  DateTime _expiryDate;
  Timer _authTimer;
  User _currentUser;

  // Function signInWithEmailAndPassword
  Future<void> signInWithEmailAndPassword(String email, String password) async {
    // Login http request

    try {
      final responseData =
          await _dioService.postData(url: _api.loginUrl, params: {
        'email': email,
        'password': password,
      });

      // check error
      if (responseData['error'] != null) {
        throw Exception(responseData['error']);
      }

      // set token, expiryDate
      _token = responseData['access_token'];
      _expiryDate = DateTime.now().add(
        Duration(
          seconds: responseData['expires_in'],
        ),
      );
      // Autologout
      _autoLogout();
      // Store data local
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode({
        'token': _token,
        'expiryDate': _expiryDate.toIso8601String(),
      });
      prefs.setString('userData', userData);
    } on TimeoutException catch (e) {
      throw ('Lỗi kết nối mạng, vui lòng kiểm tra');
    } on SocketException catch (e) {
      throw ('Server không phải hồi, vui lòng kiểm tra');
    } catch (exception, stackTrace) {
      // await Sentry.captureException(
      //   exception,
      //   stackTrace: stackTrace,
      // );
      throw (exception);
    }
  }

  // Function createUserWithEmailAndPassword
  Future<bool> createUserWithEmailAndPassword(
      {String email,
      String username,
      String password,
      String confirmPassword,
      String fullName}) async {
    try {
      final responseData =
          await _dioService.postData(url: _api.registerUrl, params: {
        'email': email,
        'username': username,
        'password': password,
        'confirm_password': confirmPassword,
        'full_name': fullName,
      });

      if (responseData['status'] == false) {
        throw Exception(responseData['message']);
      }
      if (responseData['error'] != null) {
        throw Exception(responseData['error']['message']);
      }
     // set token, expiryDate
      _token = responseData['access_token'];
      _expiryDate = DateTime.now().add(
        Duration(
          seconds: responseData['expires_in'],
        ),
      );
      // Autologout
      _autoLogout();
      // Store data local
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode({
        'token': _token,
        'expiryDate': _expiryDate.toIso8601String(),
      });
      prefs.setString('userData', userData);
      return true;
    } catch (exception, stackTrace) {
      // await Sentry.captureException(
      //   exception,
      //   stackTrace: stackTrace,
      // );
      throw (exception);
    }
  }

  // Function check user is logged
  Future<bool> isSignedIn() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      if (!prefs.containsKey('userData')) {
        return false;
      }
      final extractedUserData =
          json.decode(prefs.getString('userData')) as Map<String, Object>;
      final expiryDate = DateTime.parse(extractedUserData['expiryDate']);
      if (expiryDate.isBefore(DateTime.now())) {
        return false;
      }
      _token = extractedUserData['token'];
      _expiryDate = expiryDate;
      return true;
    } catch (e) {
      throw (e);
    }
  }

  // Function login user when reload ap
  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);
    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extractedUserData['token'];
    _expiryDate = expiryDate;
    return true;
  }

  Future<void> logout() async {
    _token = null;
    _currentUser = null;
    _expiryDate = null;

    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('userData');
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer.cancel();
    }
    final _timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: _timeToExpiry), logout);
  }
}
