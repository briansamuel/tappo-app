import 'package:tappo/components/text_field_container.dart';
import 'package:tappo/styles/constants.dart';
import 'package:flutter/material.dart';

class RoundedPasswordField extends StatefulWidget {
  final TextEditingController controller;
  final String validatorText;
  const RoundedPasswordField({Key key, this.controller, this.validatorText})
      : super(key: key);
  @override
  _RoundedPasswordFieldState createState() => _RoundedPasswordFieldState();
}

class _RoundedPasswordFieldState extends State<RoundedPasswordField> {
  TextEditingController _controller;
  String _validatorText;
  bool _showPassword = true;

  @override
  void initState() {
    _controller = widget.controller;
    _validatorText = widget.validatorText;
    super.initState();
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    _controller = widget.controller;
    _validatorText = widget.validatorText;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFieldContainer(
          child: TextFormField(
            controller: _controller,
            cursorColor: kPrimaryColor,
            decoration: InputDecoration(
              hintText: "Password",
              icon: Icon(
                Icons.lock,
                color: kPrimaryColor,
              ),
              suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      _showPassword = !_showPassword;
                    });
                  },
                  child: Icon(
                    Icons.visibility,
                    color: kPrimaryColor,
                  )),
              border: InputBorder.none,
            ),
            obscureText: _showPassword,
            autovalidate: true,
            autocorrect: false,
          ),
        ),
        AnimatedContainer(
          child: Text(
            _validatorText ?? '',
            style: TextStyle(
              color: Colors.red,
              fontSize: 12,
            ),
          ),
          height: _validatorText != null ? 20 : 0,
          duration: Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
        )
      ],
    );
  }
}
