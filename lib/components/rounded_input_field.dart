import 'package:tappo/components/text_field_container.dart';
import 'package:tappo/styles/constants.dart';
import 'package:flutter/material.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText, validatorText;
  final Function validator;
  final IconData icon;
  final ValueChanged<String> onChanged;
  final TextEditingController controller;
  const RoundedInputField({
    Key key,
    this.hintText,
    this.validatorText,
    this.validator,
    this.icon = Icons.person,
    this.onChanged,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        TextFieldContainer(
          child: TextFormField(
            controller: controller,
            // onChanged: onChanged,
            cursorColor: kPrimaryColor,
            decoration: InputDecoration(
              icon: Icon(
                icon,
                color: kPrimaryColor,
              ),
              hintText: hintText,
              border: InputBorder.none,
            ),
            keyboardType: TextInputType.emailAddress,
            autovalidate: true,
            autocorrect: false,
          ),
        ),
        AnimatedContainer(
          child: Text(
            validatorText ?? '',
            style: TextStyle(
              color: Colors.red,
              fontSize: 12,
            ),
          ),
          height: validatorText != null ? 20 : 0,
          duration: Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
        )
      ],
    );
  }
}
