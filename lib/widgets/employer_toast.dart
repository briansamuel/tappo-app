import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TappoToast extends StatelessWidget {
  final String message;
  Color color = Colors.red.shade300;
  Icon icon = Icon(Icons.error);
  TappoToast({this.message = '', this.color = Colors.red, this.icon});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: color,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          icon,
          SizedBox(
            width: 12.0,
          ),
          Flexible(
              child: Text(
            message,
            maxLines: 3,
          )),
        ],
      ),
    );
  }
}
