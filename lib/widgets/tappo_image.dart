import 'package:flutter/widgets.dart';

class TappoImage extends StatelessWidget {
  final String baseUrl = 'https://tappo.xyz/';
  final String urlImage;

  TappoImage(this.urlImage);
  @override
  Widget build(BuildContext context) {
    return Image.network((baseUrl + urlImage));
  }
}
