import 'package:flutter/material.dart';
import 'package:tappo/models/social.dart';
import 'package:reorderables/reorderables.dart';

class ListSocialWidget extends StatefulWidget {
  final List<Social> socials;
  ListSocialWidget({this.socials});
  @override
  _ListSocialWidgetState createState() => _ListSocialWidgetState();
}

class _ListSocialWidgetState extends State<ListSocialWidget> {
  List<Social> _socials;
  List<Widget> _rows = [];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _onReorder(int oldIndex, int newIndex) {
      setState(() {
        Widget row = _rows.removeAt(oldIndex);
        _rows.insert(newIndex, row);
        Social social = widget.socials.removeAt(oldIndex);
        widget.socials.insert(newIndex, social);
      });
    }

    _rows.clear();
    widget.socials.asMap().forEach((index, social) {
      _rows.add(_buttonSocial(social, index));
    });
    return Container(
        child: ReorderableColumn(
      buildDraggableFeedback: (context, constraints, child) {
        ValueKey key = child.key;
        return child;
      },
      needsLongPressDraggable: false,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: _rows,
      onReorder: _onReorder,
    ));
  }
}

Widget _buttonSocial(Social social, int key) => GestureDetector(
      key: ValueKey(key),
      onLongPress: () {},
      child: Container(
        key: ValueKey(key),
        width: 300,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(28.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 8.0,
              offset: Offset(0.0, 5.0),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CircleAvatar(
                backgroundImage:
                    AssetImage('assets/socials/' + social.icon + '.png'),
              ),
              Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Text(
                    social.label,
                    style: TextStyle(color: Colors.grey, fontSize: 20),
                  )),
            ],
          ),
        ),
      ),
    );
