import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:tappo/blocs/authentication/authentication_event.dart';
import 'package:tappo/blocs/change_password/change_password_bloc.dart';
import 'package:tappo/blocs/change_password/change_password_event.dart';
import 'package:tappo/blocs/home/home_bloc.dart';
import 'package:tappo/blocs/home/home_event.dart';
import 'package:tappo/blocs/profile/profile_bloc.dart';
import 'package:tappo/blocs/settings/setting_bloc.dart';
import 'package:tappo/blocs/settings/setting_event.dart';
import 'package:tappo/blocs/social/social_bloc.dart';
import 'package:tappo/blocs/social/social_event.dart';
import 'package:tappo/repositories/auth_repository.dart';

import 'package:tappo/repositories/user_repository.dart';
import 'package:tappo/views/main_screen.dart';

import 'blocs/authentication/authentication_bloc.dart';
import 'blocs/authentication/authentication_state.dart';

import 'blocs/login/login_bloc.dart';
import 'views/login/login_screen.dart';
import 'views/splash_screen.dart';

import 'styles/constants.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SentryFlutter.init(
    (options) {
      options.dsn =
          'https://38079aca67d4476bbab2e39662d8b378@o298411.ingest.sentry.io/5935627';
    },
    appRunner: () => runApp(EasyLocalization(
      startLocale: Locale('vi', 'VN'),
      supportedLocales: [Locale('en', 'US'), Locale('vi', 'VN')],
      path: 'assets/translations',
      // <-- change patch to your
      fallbackLocale: Locale('vi', 'VN'),
      child: TappoApp(),
    )),
  );
}

class TappoApp extends StatefulWidget {
  @override
  _TappoApp createState() => _TappoApp();
}

class _TappoApp extends State<TappoApp> {
  final AuthRepository _authRepository = AuthRepository();
  final UserRepository _userRepository = UserRepository();

  @override
  void initState() {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 2000)
      ..indicatorType = EasyLoadingIndicatorType.fadingCube
      ..loadingStyle = EasyLoadingStyle.custom
      ..indicatorSize = 45.0
      ..radius = 10.0
      ..progressColor = mpGreen
      ..backgroundColor = Colors.white
      ..indicatorColor = mpPurple
      ..textColor = mpPurple
      ..maskColor = Colors.grey.withOpacity(0.5)
      ..userInteractions = true
      ..dismissOnTap = false;
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (BuildContext context) => AuthenticationBloc(
              authRepository: _authRepository, userRepository: _userRepository)
            ..add(AuthenticationEventStarted()),
        ),
      ],
      child: MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        builder: EasyLoading.init(),
        debugShowCheckedModeBanner: false,
        home: MultiBlocProvider(
            providers: [
              // BlocProvider(
              //   create: (context) => HomeBloc(userRepository: _userRepository)
              //     ..add(HomeEventInitial()),
              // ),
              BlocProvider(
                create: (context) =>
                    ProfileBloc(userRepository: _userRepository),
              ),
              BlocProvider(
                create: (context) => SocialBloc(userRepository: _userRepository)
                  ..add(SocialEventInitial()),
              ),

              BlocProvider(
                create: (context) =>
                    ChangePasswordBloc(userRepository: _userRepository)
                      ..add(ChangePasswordEventInitial()),
              )
            ],
            child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
              builder: (context, authenticationState) {
                if (authenticationState is AuthenticationStateSuccess) {
                  final currentUser = authenticationState.user;
                  return MultiBlocProvider(
                    providers: [
                      BlocProvider(
                        create: (context) =>
                            HomeBloc(userRepository: _userRepository)
                              ..add(HomeEventInitial()),
                      ),
                      BlocProvider(
                        create: (context) =>
                            SettingBloc(userRepository: _userRepository)
                              ..add(SettingEventInitial()),
                      ),
                    ],
                    child: MainScreen(
                      user: currentUser,
                      userRepository: _userRepository,
                    ),
                  );
                } else if (authenticationState is AuthenticationStateFailure) {
                  return BlocProvider<LoginBloc>(
                    create: (context) =>
                        LoginBloc(authRepository: _authRepository),
                    child: LoginScreen(authRepository: _authRepository),
                  );
                }
                return SplashScreen();
              },
            )),
      ),
    );
    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //   home: HomePage(),
    // );
  }
}
