import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tappo/blocs/authentication/authentication_bloc.dart';
import 'package:tappo/blocs/authentication/authentication_event.dart';
import 'package:tappo/blocs/authentication/authentication_state.dart';

import 'package:tappo/blocs/login/login_bloc.dart';
import 'package:tappo/blocs/login/login_event.dart';
import 'package:tappo/blocs/login/login_state.dart';
import 'package:tappo/blocs/register/register_bloc.dart';
import 'package:tappo/blocs/register/register_event.dart';

import 'package:tappo/components/already_have_an_account_acheck.dart';
import 'package:tappo/components/rounded_button.dart';
import 'package:tappo/components/rounded_input_field.dart';
import 'package:tappo/components/rounded_password_field.dart';
import 'package:tappo/repositories/auth_repository.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tappo/views/register/register_screen.dart';
import 'package:tappo/widgets/employer_toast.dart';

import 'components/background.dart';

// import 'components/body.dart';

class LoginScreen extends StatefulWidget {
  final AuthRepository _authRepository;

  // contructor
  LoginScreen({Key key, @required AuthRepository authRepository})
      : assert(authRepository != null),
        _authRepository = authRepository,
        super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passController = TextEditingController();

  LoginBloc _loginBloc;
  AuthenticationBloc _authenticationBloc;
  AuthRepository get _authRepository => widget._authRepository;
  FToast fToast;
  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    fToast = FToast();
    fToast.init(context);
    // _emailController.text = 'dinhvanvu94@gmail.com';
    // _passController.text = 'admin@321';
    _emailController.text = '';
    _passController.text = '';
    _emailController.addListener(() {
      //
      _loginBloc.add(LoginEventEmailChanged(email: _emailController.text));
    });
    _passController.addListener(() {
      //
      _loginBloc.add(LoginEventPasswordChanged(password: _passController.text));
    });
  }

  // Kiểm tra input Empty
  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passController.text.isNotEmpty;

  // Kiểm tra Button Login Enable
  bool isLoginButtonEnable(LoginState loginState) =>
      loginState.isValidEmailAndPassword &&
      isPopulated &&
      !loginState.isSubmitting;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body:
          BlocConsumer<LoginBloc, LoginState>(listener: (context, loginState) {
        if (loginState.isFailure) {
          fToast.showToast(
            child: TappoToast(
              color: Colors.red,
              icon: Icon(Icons.close),
              message:
                  loginState.message ?? 'Có lỗi xảy ra, vui lòng đăng nhập lại',
            ),
            gravity: ToastGravity.BOTTOM,
            toastDuration: Duration(seconds: 2),
          );
          EasyLoading.dismiss();
        } else if (loginState.isSubmitting) {
          EasyLoading.show();
        } else if (loginState.isSuccess) {
          _authenticationBloc..add(AuthenticationEventLoggedIn());
        }
      }, builder: (context, loginState) {
        return Background(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "LOGIN",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: size.height * 0.03),
                SvgPicture.asset(
                  "assets/icons/login.svg",
                  height: size.height * 0.35,
                ),
                SizedBox(height: size.height * 0.03),
                RoundedInputField(
                  hintText: "Nhập địa chỉ email của bạn",
                  controller: _emailController,
                  validatorText: loginState.isValidEmail
                      ? null
                      : 'Không đúng định dạng email',
                ), // Input Email
                RoundedPasswordField(
                  controller: _passController,
                  validatorText: loginState.isValidPassword
                      ? null
                      : 'Mật khẩu ít nhất 3 ký tự',
                ), // Input Password
                RoundedButton(
                  text: "Đăng nhập",
                  press: isLoginButtonEnable(loginState)
                      ? _onLoginEmailAndPassword
                      : null,
                ), // Button Login
                SizedBox(height: size.height * 0.03),
                AlreadyHaveAnAccountCheck(
                  press: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) {
                        return BlocProvider<RegisterBloc>(
                          create: (context) =>
                              RegisterBloc(authRepository: _authRepository)
                                ..add(RegisterEventInitial()),
                          child: RegisterScreen(),
                        );
                      }),
                    );
                  },
                ), // Component Route to Sign In or Sign Out
                BlocConsumer(
                  bloc: _authenticationBloc,
                  listener: (context, authState) {
                    if (authState is AuthenticationStateFailure) {
                      fToast.showToast(
                        child: TappoToast(
                          color: Colors.red,
                          icon: Icon(Icons.close),
                          message: authState.message,
                        ),
                        gravity: ToastGravity.BOTTOM,
                        toastDuration: Duration(seconds: 2),
                      );
                      EasyLoading.dismiss();
                    } else if (authState is AuthenticationStateSuccess) {
                      fToast.showToast(
                        child: TappoToast(
                          color: Colors.green,
                          icon: Icon(Icons.check),
                          message: 'Đăng nhập thành công',
                        ),
                        gravity: ToastGravity.BOTTOM,
                        toastDuration: Duration(seconds: 2),
                      );

                      EasyLoading.dismiss();
                    }
                  },
                  builder: (context, authState) => Container(),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  void _onLoginEmailAndPassword() {
    _loginBloc.add(LoginEventWithEmailAndPasswordPressed(
      email: _emailController.text,
      password: _passController.text,
    ));
  }

  void _onLoginGoogleAccount() {
    _loginBloc.add(LoginEventWithGooglePressed());
  }
}
