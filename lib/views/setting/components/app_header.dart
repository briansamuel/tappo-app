import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tappo/styles/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';

class AppHeader extends StatelessWidget {
  final String username;
  final String avtarUrl;
  final bool isLogged;
  final int id;
  const AppHeader(
      {this.username, this.avtarUrl, this.isLogged = false, this.id = 0});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 140,
          alignment: Alignment.topLeft,
          padding: EdgeInsets.only(top: 35, left: 15, right: 15),
          width: double.infinity,
          decoration: BoxDecoration(color: Colors.purple.shade400),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    avtarUrl != null
                        ? Container(
                            height: 50,
                            child: BoxUser(
                              avatar: avtarUrl,
                              name: username,
                              size: 50,
                              id: id,
                            ),
                          )
                        : Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Icon(
                              Icons.supervised_user_circle,
                              size: 35,
                              color: mpPurple,
                            ),
                          ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      (username != null && username != "")
                          ? username
                          : "setting_screen.welcome_title".tr(),
                      style: TextStyle(height: 1.5, color: Colors.white),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class BoxUser extends StatelessWidget {
  String name;
  String avatar;
  int id;
  double size;
  BoxUser({this.name = '', this.avatar = '', this.id = 0, this.size = 45});
  final List<Color> colorList = [
    mpGreen,
    mpBlueD,
    mpYellowD,
    mpOrangeR,
    mpGreen,
    mpBlueD,
    mpYellowD,
    mpOrangeR,
    mpBlueD,
    mpGreen
  ];
  Widget build(BuildContext context) {
    String subId = id.toString().substring(id.toString().length - 1);
    id = int.parse(subId);
    return Stack(
      children: [
        Container(
          width: size,
          height: size,
          margin: EdgeInsets.only(left: 5, right: 5),
          decoration: avatar != ""
              ? BoxDecoration(
                  border: Border.all(width: 1, color: Colors.white),
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.white,
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(avatar),
                      fit: BoxFit.cover,
                      alignment: Alignment.topCenter),
                )
              : BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: colorList[id].withOpacity(0.2),
                ),
        ),
        avatar.isEmpty
            ? Positioned(
                left: 0,
                child: Container(
                  height: size,
                  width: size + 10,
                  alignment: Alignment.center,
                  child: Text(
                    name.substring(0, 2),
                    style: TextStyle(
                        fontSize: size / 2.8,
                        fontWeight: FontWeight.w600,
                        color: colorList[id]),
                  ),
                ))
            : Container(),
      ],
    );
  }
}
