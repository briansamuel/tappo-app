import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tappo/styles/constants.dart';
import 'package:tappo/styles/tappo_menu_icon.dart';

// ignore: must_be_immutable
class BoxFunction extends StatelessWidget {
  IconData icon;
  String title;
  Widget custom;
  Function function;
  bool divider;
  bool noBg;
  BoxFunction(
      {this.icon,
      this.title = "",
      this.custom,
      this.function,
      this.noBg = false,
      this.divider = false});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: function,
      padding: EdgeInsets.zero,
      color: noBg ? Color(0xffEFF7ED) : Colors.white,
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            children: [
              Container(
                width: 40,
                height: 40,
                margin: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
                decoration: BoxDecoration(
                    color: mpPurple, borderRadius: BorderRadius.circular(5)),
                child: Icon(
                  icon != null ? icon : BabySitterIcon.email,
                  size: 30,
                  color: Colors.white,
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(title),
                    custom != null ? custom : Container(),
                  ],
                ),
              ),
              Icon(
                Icons.keyboard_arrow_right,
                color: mpBtGray,
              ),
            ],
          ),
          divider
              ? SizedBox(
                  width: MediaQuery.of(context).size.width - 60,
                  height: 5,
                  child: Divider(),
                )
              : Container()
        ],
      ),
    );
  }
}
