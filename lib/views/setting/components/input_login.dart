import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tappo/styles/constants.dart';

// ignore: must_be_immutable
class InputField extends StatelessWidget {
  bool isPass;
  String placeholder;
  @required
  TextEditingController textEditingController;
  IconData icon;
  Function checkPass;
  Function onChanged;
  String errorText;
  @required
  FocusNode focusNode;
  bool viewPass;

  InputField(
      {this.isPass = false,
      this.viewPass = false,
      this.placeholder = "",
      this.icon = Icons.email,
      this.textEditingController,
      this.errorText = "",
      this.onChanged,
      this.focusNode,
      this.checkPass});

  @override
  Widget build(BuildContext context) {
    // ignore: unnecessary_statements
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            color: mpbgGray,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              SizedBox(
                width: 10,
              ),
              Icon(
                icon,
                color: mpBgPurple,
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                color: mpBgPurple,
                height: 30,
                width: 2,
              ),
              Expanded(
                child: TextField(
                  focusNode: focusNode,
                  obscureText: isPass
                      ? !viewPass
                          ? true
                          : false
                      : false,
                  controller: textEditingController,
                  style: TextStyle(fontSize: 15.5),
                  // ignore: unnecessary_statements
                  onChanged: (value) => onChanged(),
                  decoration: InputDecoration(
                    hintText: placeholder,
                    contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                  ),
                ),
              ),
              isPass
                  ? InkWell(
                      child: Icon(
                        !viewPass
                            ? Icons.remove_red_eye_outlined
                            : Icons.remove_red_eye,
                        color: mpPurple,
                      ),
                      onTap: checkPass,
                    )
                  : Container(),
              SizedBox(
                width: 10,
              ),
            ],
          ),
        ),
        errorText != null
            ? Column(
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    errorText,
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  )
                ],
              )
            : Container(),
      ],
    );
  }
}
