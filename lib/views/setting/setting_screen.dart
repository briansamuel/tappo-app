import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:tappo/blocs/authentication/authentication_bloc.dart';
import 'package:tappo/blocs/authentication/authentication_event.dart';
import 'package:tappo/blocs/change_password/change_password_bloc.dart';
import 'package:tappo/blocs/home/home_bloc.dart';
import 'package:tappo/blocs/home/home_event.dart';
import 'package:tappo/blocs/profile/profile_bloc.dart';
import 'package:tappo/blocs/profile/profile_event.dart';
import 'package:tappo/blocs/settings/setting_bloc.dart';
import 'package:tappo/blocs/settings/setting_state.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/repositories/user_repository.dart';
import 'package:tappo/styles/constants.dart';
import 'package:tappo/configs/api.dart';
import 'package:tappo/styles/tappo_menu_icon.dart';
import 'package:tappo/views/setting/child_screens/background_screen.dart';
import 'package:tappo/views/setting/child_screens/change_password_screen.dart';
import 'package:tappo/views/setting/child_screens/nfc_screen.dart';
import 'package:tappo/views/setting/child_screens/qrcode_screen.dart';
import 'package:tappo/views/setting/components/app_header.dart';
import 'package:easy_localization/easy_localization.dart';

import 'components/box_function.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  bool lockInBackground = true;
  bool notificationsEnabled = true;
  SettingBloc _settingBloc;
  AuthenticationBloc _authenticationBloc;
  ProfileBloc _profileBloc;
  ChangePasswordBloc _changePasswordBloc;
  HomeBloc _homeBloc;
  final Api _api = new Api();
  UserRepository _userRepository;
  @override
  void initState() {
    super.initState();
    _settingBloc = BlocProvider.of<SettingBloc>(context);
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _profileBloc = BlocProvider.of<ProfileBloc>(context);
    _changePasswordBloc = BlocProvider.of<ChangePasswordBloc>(context);
    _homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mpBgGreen.withOpacity(0.05),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            BlocBuilder(
              bloc: _settingBloc,
              builder: (context, state) {
                if (state is SettingStateSuccess) {
                  return AppHeader(
                    avtarUrl: state.user.avatarUrl != null
                        ? _api.homeUrl + state.user.avatarUrl
                        : '',
                    username: state.user.fullname,
                  );
                }
                return AppHeader();
              },
            ),
            SizedBox(
              height: 10,
            ),
            BlocBuilder(
              bloc: _settingBloc,
              builder: (context, state) {
                User user = null;
                if (state is SettingStateSuccess) {
                  user = state.user;
                }
                return tabProfileWidget(user);
              },
            ),
            SizedBox(
              height: 10,
            ),
            logoutWidget(),
          ],
        ),
      )),
    );
  }

  Widget tabProfileWidget(User user) {
    return Column(
      children: [
        Divider(height: 2),
        // BoxFunction(
        //   title: "setting_screen.menu_change_languages".tr(),
        //   icon: BabySitterIcon.toggle,
        //   function: () {},
        // ),
        BoxFunction(
          title: "setting_screen.menu_change_background".tr(),
          icon: BabySitterIcon.term,
          function: () {
            pushNewScreen(
              context,
              screen: BlocProvider.value(
                value: _profileBloc..add(ProfileEventInitial()),
                child: BackgroundScreen(),
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              pageTransitionAnimation: PageTransitionAnimation.cupertino,
            ).then((value) {
              _homeBloc..add(HomeEventLoaded());
            });
          },
        ),

        BoxFunction(
          title: "setting_screen.menu_qrcode".tr(),
          icon: Icons.qr_code,
          function: () {
            pushNewScreen(
              context,
              screen: QrCodeScreen(user: user),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              pageTransitionAnimation: PageTransitionAnimation.cupertino,
            ).then((value) {
              _homeBloc..add(HomeEventLoaded());
            });
          },
        ),

        BoxFunction(
          title: "setting_screen.menu_nfc_card".tr(),
          icon: Icons.tag,
          function: () {
            pushNewScreen(
              context,
              screen: NfcScreen(user: user),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              pageTransitionAnimation: PageTransitionAnimation.cupertino,
            ).then((value) {
              EasyLoading.dismiss();
              EasyLoading.instance
                ..indicatorType = EasyLoadingIndicatorType.fadingCube;
            });
          },
        ),

        // BoxFunction(
        //   title: "setting_screen.menu_notify_label".tr(),
        //   icon: BabySitterIcon.notify,
        //   function: () {},
        // ),
        BoxFunction(
          title: "setting_screen.menu_change_password".tr(),
          icon: BabySitterIcon.user,
          function: () {
            pushNewScreen(
              context,
              screen: BlocProvider.value(
                value: _changePasswordBloc,
                child: ChangePasswordScreen(),
              ),
              withNavBar: false, // OPTIONAL VALUE. True by default.
              pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
        ),
      ],
    );
  }

  Widget logoutWidget() {
    return Container(
      child: Column(
        children: [
          Divider(
            height: 1,
          ),
          BoxFunction(
            title: "setting_screen.menu_logout".tr(),
            icon: BabySitterIcon.logout,
            function: () {
              _authenticationBloc..add(AuthenticationEventLoggedOut());
            },
          ),
          Divider(height: 1),
        ],
      ),
    );
  }
}
