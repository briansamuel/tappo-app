import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:tappo/configs/api.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/styles/constants.dart';

class QrCodeScreen extends StatefulWidget {
  final User user;
  QrCodeScreen({this.user});
  @override
  _QrCodeScreenState createState() => _QrCodeScreenState();
}

class _QrCodeScreenState extends State<QrCodeScreen> {
  User _user;
  Api _api = new Api();
  @override
  void initState() {
    // TODO: implement initState
    _user = widget.user;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            stops: [
              0.1,
              0.4,
              0.6,
              0.9,
            ],
            colors: [
              Colors.yellow,
              Colors.red,
              Colors.indigo,
              Colors.teal,
            ],
          ),
        ),
        child: Container(
          constraints: new BoxConstraints(
            minHeight: 5.0,
            minWidth: 5.0,
            maxHeight: 30.0,
            maxWidth: 30.0,
          ),
          child: new DecoratedBox(
            decoration: new BoxDecoration(
                // color: Colors.white,
                ),
            child: Center(
                child: QrImage(
              data: _api.homeUrl + _user.qrCode.toString(),
              version: QrVersions.auto,
              backgroundColor: Colors.white,
              foregroundColor: mpPurple,
              dataModuleStyle:
                  QrDataModuleStyle(dataModuleShape: QrDataModuleShape.square),
              size: 280.0,
              embeddedImage: _user.avatarUrl != null
                  ? CachedNetworkImageProvider(_api.homeUrl + _user.avatarUrl)
                  : null,
              embeddedImageStyle: QrEmbeddedImageStyle(
                size: Size(40, 40),
              ),
              errorStateBuilder: (cxt, err) {
                return Container(
                  child: Center(
                    child: Text(
                      "Lỗi khi tạo QRCode, vui lòng đăng nhập lại",
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
              },
            )),
          ),
        ),
      ),
    );
  }
}
