import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tappo/blocs/change_password/change_password_bloc.dart';
import 'package:tappo/blocs/change_password/change_password_event.dart';
import 'package:tappo/blocs/change_password/change_password_state.dart';
import 'package:tappo/blocs/profile/profile_state.dart';
import 'package:tappo/styles/constants.dart';
import 'package:tappo/views/setting/components/input_login.dart';
import 'package:tappo/widgets/employer_toast.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key key}) : super(key: key);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  bool viewPass = false;
  bool viewNewPass = false;
  bool viewNewPassConfirm = false;
  ChangePasswordBloc _changePasswordBloc;
  FocusNode focusNodePassword = FocusNode();
  FocusNode focusNodePasswordNew = FocusNode();
  FocusNode focusNodePasswordNewComfirm = FocusNode();
  TextEditingController _textEditingControllerCurrentPassword =
      TextEditingController();
  TextEditingController _textEditingControllerPasswordNew =
      TextEditingController();
  TextEditingController _textEditingControllerPasswordConfirm =
      TextEditingController();
  Map<String, String> _dataPass = {};
  FToast fToast;
  @override
  void initState() {
    super.initState();
    _changePasswordBloc = BlocProvider.of<ChangePasswordBloc>(context);
    fToast = FToast();
    fToast.init(context);
  }

  inputChange() {
    // Thao tác khi thực hiện việc nhập dữ liệu để loại bỏ errorText

    _dataPass['oldPassword'] = _textEditingControllerCurrentPassword.text;
    _dataPass['newPassword'] = _textEditingControllerPasswordNew.text;
    _dataPass['newPasswordComfirm'] =
        _textEditingControllerPasswordConfirm.text;
    _changePasswordBloc
      ..add(ChangePasswordEventInputChange(dataPass: _dataPass));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // Set = true để tránh việc khi nhấn vào ô TextField bàn phím hiện lên nó sẽ tự động đẩy lên tránh bị đè

      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Column(children: [
            SizedBox(
              height: 80,
            ),
            Container(
              constraints: BoxConstraints(
                maxWidth: 350,
              ),
              child: Text(
                'change_password_screen.slogan'.tr(),
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              constraints: BoxConstraints(
                maxWidth: 300,
              ),
              child: Column(
                children: [
                  BlocConsumer(
                      bloc: _changePasswordBloc,
                      listener: (context, state) {
                        if (state is ChangePasswordStateFailed) {
                          fToast.showToast(
                            child: TappoToast(
                              color: Colors.red,
                              icon: Icon(Icons.close),
                              message: state.message,
                            ),
                            gravity: ToastGravity.BOTTOM,
                            toastDuration: Duration(seconds: 2),
                          );
                          EasyLoading.dismiss();
                        } else if (state is ChangePasswordStateSuccess) {
                          fToast.showToast(
                            child: TappoToast(
                              color: Colors.green,
                              icon: Icon(Icons.check),
                              message: 'Đổi mật khẩu thành công',
                            ),
                            gravity: ToastGravity.BOTTOM,
                            toastDuration: Duration(seconds: 2),
                          );

                          EasyLoading.dismiss();
                        } else if (state is ChangePasswordStateLoading) {
                          EasyLoading.show();
                        }
                      },
                      builder: (context, state) {
                        String errTextPassword;
                        if (state is ProfileStateFailed) {
                        } else {
                          if (state is ChangePasswordStateInputChange &&
                              !state.validNewPassword) {
                            errTextPassword = 'Mật khẩu có ít nhất 3 ký tự';
                          }
                        }
                        return InputField(
                          icon: Icons.vpn_key_sharp,
                          onChanged: inputChange,
                          errorText: errTextPassword,
                          placeholder:
                              "change_password_screen.password_input".tr(),
                          textEditingController:
                              _textEditingControllerCurrentPassword,
                          isPass: true,
                          focusNode: focusNodePassword,
                          viewPass: (state is ChangePasswordStateShowHide)
                              ? state.isShowHidePassword
                              : viewPass,
                          checkPass: () {
                            setState(() {
                              viewPass = !viewPass;
                            });
                          },
                        );
                      }),
                  SizedBox(
                    height: 10,
                  ),
                  BlocBuilder(
                      bloc: _changePasswordBloc,
                      builder: (context, state) {
                        String errTextPassword;
                        if (state is ProfileStateFailed) {
                        } else {
                          if (state is ChangePasswordStateInputChange &&
                              !state.validNewPassword) {
                            errTextPassword = 'Mật khẩu có ít nhất 3 ký tự';
                          }
                        }

                        return InputField(
                          icon: Icons.lock_rounded,
                          onChanged: inputChange,
                          placeholder:
                              "change_password_screen.new_password_input".tr(),
                          textEditingController:
                              _textEditingControllerPasswordNew,
                          errorText: errTextPassword,
                          isPass: true,
                          focusNode: focusNodePasswordNew,
                          viewPass: viewNewPass,
                          checkPass: () {
                            setState(() {
                              viewNewPass = !viewNewPass;
                            });
                          },
                        );
                      }),
                  SizedBox(
                    height: 10,
                  ),
                  BlocBuilder(
                      bloc: _changePasswordBloc,
                      builder: (context, state) {
                        String errTextPassword;
                        if (state is ProfileStateFailed) {
                        } else {
                          if (state is ChangePasswordStateInputChange &&
                              !state.validNewPasswordConfirm) {
                            errTextPassword = 'Xác nhận mật khẩu không khớp';
                          }
                        }
                        return InputField(
                          icon: Icons.lock_rounded,
                          onChanged: inputChange,
                          placeholder:
                              "change_password_screen.new_password_comfirm_input"
                                  .tr(),
                          textEditingController:
                              _textEditingControllerPasswordConfirm,
                          errorText: errTextPassword,
                          isPass: true,
                          focusNode: focusNodePasswordNewComfirm,
                          viewPass: viewNewPassConfirm,
                          checkPass: () {
                            setState(() {
                              viewNewPassConfirm = !viewNewPassConfirm;
                            });
                          },
                        );
                      }),
                  SizedBox(
                    height: 30,
                  ),
                  FlatButton(
                    onPressed: () {
                      _dataPass['oldPassword'] =
                          _textEditingControllerCurrentPassword.text;
                      _dataPass['newPassword'] =
                          _textEditingControllerPasswordNew.text;
                      _dataPass['newPasswordComfirm'] =
                          _textEditingControllerPasswordConfirm.text;
                      _changePasswordBloc
                        ..add(
                            ChangePasswordEventPressSave(dataPass: _dataPass));
                    },
                    minWidth: MediaQuery.of(context).size.width - 80,
                    color: mpPurple,
                    padding: EdgeInsets.all(20),
                    child: Text(
                      "change_password_screen.change_password".tr(),
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
