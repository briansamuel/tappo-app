import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:tappo/configs/api.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/widgets/employer_toast.dart';

class NfcScreen extends StatefulWidget {
  final User user;
  NfcScreen({this.user});

  @override
  _NfcScreenState createState() => _NfcScreenState();
}

class _NfcScreenState extends State<NfcScreen> {
  FToast fToast;
  ValueNotifier<dynamic> result = ValueNotifier(null);
  Api _api = new Api();
  User _user;
  void initState() {
    fToast = FToast();
    fToast.init(context);
    _user = widget.user;
    EasyLoading.instance..indicatorType = EasyLoadingIndicatorType.cubeGrid;
    super.initState();
    // Check if the device supports NFC reading
  }

  void _tagRead() {
    EasyLoading.show(status: 'Vui lòng để thẻ gần thiết bị');
    NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      result.value = tag.data;
      NfcManager.instance.stopSession();
      EasyLoading.dismiss();
    });
  }

  void _ndefWrite() {
    EasyLoading.show(status: 'Vui lòng để thẻ gần thiết bị');
    Future.delayed(Duration(seconds: 30)).then((value) {
      NfcManager.instance.stopSession();
      EasyLoading.dismiss();
    });
    NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      var ndef = Ndef.from(tag);
      if (ndef == null || !ndef.isWritable) {
        NfcManager.instance.stopSession(errorMessage: result.value);
        EasyLoading.dismiss();
        fToast.showToast(
          child: TappoToast(
            color: Colors.green,
            icon: Icon(Icons.check),
            message: 'Thẻ đã bị khoá, vui lòng mở khoá',
          ),
          gravity: ToastGravity.BOTTOM,
          toastDuration: Duration(seconds: 1),
        );
        return;
      }

      NdefMessage message = NdefMessage([
        NdefRecord.createUri(Uri.parse(_api.homeUrl + _user.qrCode.toString())),
      ]);

      try {
        await ndef.write(message);

        NfcManager.instance.stopSession();
        EasyLoading.dismiss();
        fToast.showToast(
          child: TappoToast(
            color: Colors.green,
            icon: Icon(Icons.check),
            message: 'Đã chép dữ liệu thành công',
          ),
          gravity: ToastGravity.BOTTOM,
          toastDuration: Duration(seconds: 1),
        );
      } catch (e) {
        NfcManager.instance.stopSession(errorMessage: result.value.toString());
        EasyLoading.dismiss();
        fToast.showToast(
          child: TappoToast(
            color: Colors.green,
            icon: Icon(Icons.check),
            message: 'Thất bại, vui lòng thử lại',
          ),
          gravity: ToastGravity.BOTTOM,
          toastDuration: Duration(seconds: 1),
        );
        return;
      }
    });
  }

  void _ndefWriteLock() {
    NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      var ndef = Ndef.from(tag);
      if (ndef == null) {
        result.value = 'Tag is not ndef';
        NfcManager.instance.stopSession(errorMessage: result.value.toString());
        return;
      }

      try {
        await ndef.writeLock();
        result.value = 'Success to "Ndef Write Lock"';
        NfcManager.instance.stopSession();
      } catch (e) {
        result.value = e;
        NfcManager.instance.stopSession(errorMessage: result.value.toString());
        return;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              stops: [
                0.1,
                0.4,
                0.6,
                0.9,
              ],
              colors: [
                Colors.yellow,
                Colors.red,
                Colors.indigo,
                Colors.teal,
              ],
            ),
          ),
          child: Container(
            constraints: new BoxConstraints(
              minHeight: 5.0,
              minWidth: 5.0,
              maxHeight: 30.0,
              maxWidth: 30.0,
            ),
            child: new DecoratedBox(
              decoration: new BoxDecoration(
                  // color: Colors.white,
                  ),
              child: FutureBuilder(
                future: NfcManager.instance.isAvailable(),
                builder: (context, ss) => ss.data != true
                    ? Center(
                        child: Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            margin: EdgeInsets.all(10),
                            child: Text(
                              'Thiết bị chưa bật NFC',
                              style: TextStyle(fontSize: 20),
                            )),
                      )
                    : Center(
                        child: Container(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/images/nfc-image.png',
                              width: double.infinity,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: Container(
                                constraints: BoxConstraints(
                                    maxWidth: 200,
                                    minHeight: 50.0,
                                    maxHeight: 50.0),
                                margin: EdgeInsets.all(10),
                                child: ElevatedButton(
                                  onPressed: _ndefWrite,
                                  style: ElevatedButton.styleFrom(
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(30.0),
                                    ),
                                  ),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    child: Text(
                                      'Đồng bộ với thẻ',
                                      style: TextStyle(fontSize: 16),
                                      textAlign: TextAlign.right,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
                      ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
