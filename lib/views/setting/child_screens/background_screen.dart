import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tappo/blocs/profile/profile_bloc.dart';
import 'package:tappo/blocs/profile/profile_event.dart';
import 'package:tappo/blocs/profile/profile_state.dart';
import 'package:tappo/styles/background.dart';
import 'package:tappo/styles/constants.dart';
import 'package:tappo/widgets/employer_toast.dart';

class BackgroundScreen extends StatefulWidget {
  const BackgroundScreen({Key key}) : super(key: key);

  @override
  _BackgroundScreenState createState() => _BackgroundScreenState();
}

class _BackgroundScreenState extends State<BackgroundScreen> {
  List<Map> _listBackground = listBackground;
  int _seletedColor = 0;
  ProfileBloc _profileBloc;
  FToast fToast;
  @override
  void initState() {
    _profileBloc = BlocProvider.of<ProfileBloc>(context);

    fToast = FToast();
    fToast.init(context);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant oldWidget) {
    _profileBloc = BlocProvider.of<ProfileBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocConsumer(
          bloc: _profileBloc,
          listener: (context, state) {
            if (state is ProfileStateFailed) {
              fToast.showToast(
                child: TappoToast(
                  color: Colors.red,
                  icon: Icon(Icons.close),
                  message: 'Đổi ảnh nền thất bại',
                ),
                gravity: ToastGravity.BOTTOM,
                toastDuration: Duration(seconds: 1),
              );
              EasyLoading.dismiss();
            } else if (state is ProfileStateSuccess && !state.isInit) {
              fToast.showToast(
                child: TappoToast(
                  color: Colors.green,
                  icon: Icon(Icons.check),
                  message: 'Đổi ảnh nền thành công',
                ),
                gravity: ToastGravity.BOTTOM,
                toastDuration: Duration(seconds: 1),
              );

              EasyLoading.dismiss();
            } else if (state is ProfileStateLoading) {
              EasyLoading.show();
            }
          },
          builder: (context, state) {
            if (state is ProfileStateSuccess) {
              _seletedColor = state.user != null ? state.user.background : 0;
            }

            return GridView.count(
              // horizontal, this produces 2 rows.
              primary: false,
              padding: const EdgeInsets.all(4),
              crossAxisSpacing: 4,
              mainAxisSpacing: 4,
              crossAxisCount: 3,
              physics: ScrollPhysics(),
              // Generate 100 widgets that display their index in the List.
              children: List.generate(_listBackground.length, (index) {
                return _listBackground[index]['colors'] != null
                    ? InkWell(
                        onDoubleTap: () {
                          _profileBloc
                            ..add(ProfileEventChangeBackground(
                                background: index));
                        },
                        child: Container(
                          padding: const EdgeInsets.all(2),
                          child: new Container(
                            decoration: new BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topRight,
                                  end: Alignment.bottomLeft,
                                  colors: _listBackground[index]['colors'],
                                ),
                                borderRadius: new BorderRadius.all(
                                    Radius.circular(10.0))),
                            child: _seletedColor == index
                                ? Icon(
                                    Icons.check_circle,
                                    color: mpPurple,
                                  )
                                : Container(),
                          ),
                        ))
                    : InkWell(
                        onDoubleTap: () {
                          _profileBloc
                            ..add(ProfileEventChangeBackground(
                                background: index));
                        },
                        child: Container(
                          padding: const EdgeInsets.all(2),
                          child: new Container(
                            decoration: new BoxDecoration(
                                color: _listBackground[index]['color'],
                                borderRadius: new BorderRadius.all(
                                    Radius.circular(10.0))),
                            child: _seletedColor == index
                                ? Icon(
                                    Icons.check_circle,
                                    color: mpPurple,
                                  )
                                : Container(),
                          ),
                        ),
                      );
              }),
            );
          },
        ),
      ),
    );
  }
}
