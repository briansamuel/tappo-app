import 'package:flutter/material.dart';
import 'package:tappo/blocs/social/social_bloc.dart';
import 'package:tappo/models/social.dart';

// ignore: must_be_immutable
class EditSocialScreen extends StatefulWidget {
  Social social;
  int index;
  Function editSocial;

  EditSocialScreen({this.social, this.index, this.editSocial});
  @override
  _EditSocialScreenState createState() => _EditSocialScreenState();
}

class _EditSocialScreenState extends State<EditSocialScreen> {
  List<String> _socialIcon = [
    'facebook',
    'messenger',
    'instagram',
    'twitter',
    'email',
    'behance',
    'linkedin',
    'pinterest',
    'skype',
    'tumblr',
    'tiktok',
    'whatsapp',
    'github',
    'vk',
    'youtube',
    'website',
    'phone',
    'sms',
  ];
  int _selectIcon = 0;
  TextEditingController _labelController = TextEditingController();
  TextEditingController _linkController = TextEditingController();
  SocialBloc _socialBloc;

  @override
  void initState() {
    _labelController.text = widget.social.label;
    _linkController.text = widget.social.content;
    _selectIcon =
        _socialIcon.indexWhere((element) => element == widget.social.icon);
    // _socialBloc = BlocProvider.of<SocialBloc>(context);
  }

  void editSocial(Social social, BuildContext ctxModal) {
    widget.editSocial(social, widget.index, ctxModal);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      height: double.infinity,
      padding: EdgeInsets.all(20),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextFormField(
              controller: _labelController,
              decoration: const InputDecoration(
                icon: Icon(Icons.person),
                labelText: 'Tên Social',
              ),
              onSaved: (String value) {
                // This optional block of code can be used to run
                // code when the user saves the form.
              },
              validator: (String value) {
                return (value != null && value.contains('@'))
                    ? 'Do not use the @ char.'
                    : null;
              },
            ),
            TextFormField(
              controller: _linkController,
              decoration: const InputDecoration(
                icon: Icon(Icons.description),
                labelText: 'Link Social',
              ),
              onSaved: (String value) {
                // This optional block of code can be used to run
                // code when the user saves the form.
              },
              validator: (String value) {
                return (value != null && value.contains('@'))
                    ? 'Do not use the @ char.'
                    : null;
              },
            ),
            new Expanded(
              flex: 2,
              child: GridView.count(
                // Create a grid with 2 columns. If you change the scrollDirection to
                // horizontal, this produces 2 rows.
                crossAxisCount: 4,
                // Generate 100 widgets that display their index in the List.
                children: List.generate(_socialIcon.length, (index) {
                  return InkWell(
                    onTap: () {
                      setState(() {
                        _selectIcon = index;
                      });
                    },
                    child: _selectIcon == index
                        ? Center(
                            child: CircleAvatar(
                              backgroundImage: AssetImage(
                                'assets/socials/' + _socialIcon[index] + '.png',
                              ),
                              child: IconButton(
                                  icon: Icon(
                                    Icons.check,
                                    size: 40.0,
                                    color: Colors.green,
                                  ),
                                  onPressed: () {}),
                            ),
                          )
                        : Center(
                            child: CircleAvatar(
                              backgroundImage: AssetImage('assets/socials/' +
                                  _socialIcon[index] +
                                  '.png'),
                            ),
                          ),
                  );
                }),
              ),
            ),
            Center(
              child: TextButton(
                onPressed: () {
                  final _newSocial = Social(
                    label: _labelController.text,
                    content: _linkController.text,
                    icon: _socialIcon[_selectIcon],
                  );
                  editSocial(_newSocial, context);
                },
                child: Text('Lưu lại'),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.blue),
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
