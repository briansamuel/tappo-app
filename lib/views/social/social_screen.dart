import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tappo/blocs/social/social_bloc.dart';
import 'package:tappo/blocs/social/social_event.dart';
import 'package:tappo/blocs/social/social_state.dart';
import 'package:tappo/models/social.dart';
import 'package:tappo/views/social/components/add_social_modal.dart';
import 'package:tappo/widgets/employer_toast.dart';
import 'package:tappo/widgets/reoder_social_widget.dart';

import 'components/edit_social_modal.dart';

class SocialScreen extends StatefulWidget {
  @override
  _SocialScreenState createState() => _SocialScreenState();
}

class _SocialScreenState extends State<SocialScreen> {
  SocialBloc _socialBloc;
  List<Social> _socials;
  List<String> _socialIcon = [
    'facebook',
    'messenger',
    'instagram',
    'twitter',
    'email',
    'behance',
    'linkedin',
    'pinterest',
    'skype',
    'tumblr',
    'tiktok',
    'whatsapp',
    'github',
    'vk',
    'youtube',
    'website',
    'phone',
    'sms',
  ];

  FToast fToast;

  @override
  void initState() {
    super.initState();
    _socialBloc = BlocProvider.of<SocialBloc>(context);
    fToast = FToast();
    fToast.init(context);
  }

  void _addSocial(Social social, BuildContext ctxModal) {
    _socialBloc..add(SocialEventAddSocial(social: social));
    Navigator.pop(ctxModal);
  }

  void _editSocial(Social social, int index, BuildContext ctxModal) {
    _socialBloc..add(SocialEventEditSocial(social: social, index: index));
    Navigator.pop(ctxModal);
  }

  void _saveSocial() {
    _socialBloc..add(SocialEventPressSave(socials: _socials));
  }

  void _changeItemFunction(List<Social> newSocials) {
    _socials = newSocials;
  }

  void openModalEdit(index) {
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext ctx) {
        return EditSocialScreen(
          social: _socials[index],
          editSocial: _editSocial,
          index: index,
        );
      },
    );
  }

  void _showModalRemoveItem(List<Social> socials, int index) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: new Icon(Icons.cancel),
                title: new Text('Huỷ'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: new Icon(Icons.edit),
                title: new Text('Sửa'),
                onTap: () {
                  Navigator.pop(context);
                  openModalEdit(index);
                },
              ),
              ListTile(
                leading: new Icon(Icons.delete),
                title: new Text('Xoá'),
                onTap: () {
                  socials.removeAt(index);
                  _socialBloc..add(SocialEventRemoveSocial(socials: socials));
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Container(
          child: BlocConsumer(
            bloc: _socialBloc,
            listener: (context, stateSocial) {
              if (stateSocial is SocialStateSuccess) {
                String message = 'Lưu thành công';
                if (stateSocial.isAddSocial) {
                  message = 'Thêm thành công';
                  _socials = stateSocial.socials;
                } else if (stateSocial.isEditSocial) {
                  message = 'Sửa thành công';
                  _socials = stateSocial.socials;
                } else {}
                fToast.showToast(
                  child: TappoToast(
                    color: Colors.green,
                    icon: Icon(Icons.check),
                    message: message,
                  ),
                  gravity: ToastGravity.BOTTOM,
                  toastDuration: Duration(seconds: 2),
                );
              } else if (stateSocial is SocialStateFailure) {
                fToast.showToast(
                  child: TappoToast(
                    color: Colors.red,
                    icon: Icon(Icons.remove),
                    message: 'Lưu thất bại',
                  ),
                  gravity: ToastGravity.BOTTOM,
                  toastDuration: Duration(seconds: 2),
                );
              }

              if (stateSocial is SocialStateloading) {
                EasyLoading.show();
              } else {
                EasyLoading.dismiss();
              }
            },
            builder: (context, stateSocial) {
              if (stateSocial is SocialStateloading) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else if (stateSocial is SocialStateSuccess ||
                  stateSocial is SocialStateInitial ||
                  stateSocial is SocialStateFailure) {
                _socials = stateSocial.socials;
                return Container(
                  height: double.infinity,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      stops: [
                        0.1,
                        0.4,
                        0.6,
                        0.9,
                      ],
                      colors: [
                        Colors.yellow,
                        Colors.red,
                        Colors.indigo,
                        Colors.teal,
                      ],
                    ),
                  ),
                  child: Stack(children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 50, left: 20, right: 20, bottom: 100),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 8.0,
                              offset: Offset(0.0, 5.0),
                            ),
                          ],
                        ),
                        width: double.infinity,
                        // constraints: new BoxConstraints(
                        //   maxHeight: double.infinity - 100,
                        // ),
                        child: SingleChildScrollView(
                          child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 0.0, bottom: 15.0),
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  // List Social
                                  ReoderSocialWidget(
                                    socials: stateSocial.socials,
                                    functionLongPress: _showModalRemoveItem,
                                    functionReoderItem: _changeItemFunction,
                                  )
                                ],
                              )),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 10,
                      left: 10,
                      right: 10,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            constraints: BoxConstraints(
                                maxWidth: double.infinity,
                                minHeight: 50.0,
                                maxHeight: 50.0),
                            margin: EdgeInsets.all(10),
                            child: ElevatedButton(
                              onPressed: () {
                                showModalBottomSheet<void>(
                                  context: context,
                                  isScrollControlled: true,
                                  builder: (BuildContext ctx) {
                                    return AddSocialScreen(
                                      addSocial: _addSocial,
                                    );
                                  },
                                );
                              },
                              style: ElevatedButton.styleFrom(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: Row(children: [
                                  Text(
                                    'Thêm mới',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  SizedBox(width: 5),
                                  Icon(Icons.add),
                                ]),
                              ),
                            ),
                          ),
                          Container(
                            constraints: BoxConstraints(
                                maxWidth: double.infinity,
                                minHeight: 50.0,
                                maxHeight: 50.0),
                            margin: EdgeInsets.all(10),
                            child: ElevatedButton(
                              onPressed: _saveSocial,
                              style: ElevatedButton.styleFrom(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: Row(children: [
                                  Text(
                                    'Lưu social',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  SizedBox(width: 5),
                                  Icon(Icons.save),
                                ]),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ]),
                );
              } else if (stateSocial is SocialStateFailure) {
                return Text('Xảy ra lỗi, vui lòng khởi động lại ứng dụng!');
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ),
      ),
    );
  }
}
