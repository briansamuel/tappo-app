import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tappo/blocs/authentication/authentication_bloc.dart';
import 'package:tappo/blocs/authentication/authentication_event.dart';
import 'package:tappo/blocs/login/login_bloc.dart';
import 'package:tappo/blocs/register/register_bloc.dart';
import 'package:tappo/blocs/register/register_event.dart';
import 'package:tappo/blocs/register/register_state.dart';
import 'package:tappo/components/already_have_an_account_acheck.dart';
import 'package:tappo/components/rounded_button.dart';
import 'package:tappo/components/rounded_input_field.dart';
import 'package:tappo/components/rounded_password_field.dart';
import 'package:tappo/repositories/user_repository.dart';
import 'package:tappo/views/login/login_screen.dart';
import 'package:tappo/widgets/employer_toast.dart';

import 'components/background.dart';
import 'components/or_divider.dart';
import 'components/social_icon.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _fullnameController = TextEditingController();
  final TextEditingController _passController = TextEditingController();
  final TextEditingController _passConfirmController = TextEditingController();
  RegisterBloc _registerBloc;
  AuthenticationBloc _authenticationBloc;
  FToast fToast;
  @override
  void initState() {
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    fToast = FToast();
    fToast.init(context);

    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    // _emailController.text = 'dinhvanvu94@gmail.com';
    // _usernameController.text = 'dinhvanvu94';
    // _fullnameController.text = 'Đinh Văn Vũ';
    // _passController.text = 'admin@123';
    // _passConfirmController.text = 'admin@123';
    _emailController.addListener(() {
      //
      _registerBloc
          .add(RegisterEventEmailChanged(email: _emailController.text));
    });
    _usernameController.addListener(() {
      //
      _registerBloc.add(
          RegisterEventUsernameChanged(username: _usernameController.text));
    });
    _passController.addListener(() {
      //
      _registerBloc.add(RegisterEventPasswordChanged(
          password: _passController.text,
          confirmPassword: _passConfirmController.text));
    });
    _passConfirmController.addListener(() {
      //
      _registerBloc.add(RegisterEventConfirmPasswordChanged(
          password: _passController.text,
          confirmPassword: _passConfirmController.text));
    });
  }

  bool get isPopulated =>
      _emailController.text.isNotEmpty &&
      _passController.text.isNotEmpty &&
      _passConfirmController.text.isNotEmpty &&
      _usernameController.text.isNotEmpty &&
      _fullnameController.text.isNotEmpty;

  bool isRegisterButtonEnable(RegisterState registerState) =>
      registerState.isValidEmailAndPassword &&
      isPopulated &&
      !registerState.isSubmitting;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
          child: BlocConsumer(
              bloc: _registerBloc,
              listener: (context, registerState) {
                if (registerState.isFailure) {
                  fToast.showToast(
                    child: TappoToast(
                      color: Colors.red,
                      icon: Icon(Icons.close),
                      message: registerState.message,
                    ),
                    gravity: ToastGravity.BOTTOM,
                    toastDuration: Duration(seconds: 2),
                  );
                  EasyLoading.dismiss();
                } else if (registerState.isSubmitting) {
                  EasyLoading.show();
                } else if (registerState.isSuccess) {
                  EasyLoading.dismiss();
                  fToast.showToast(
                    child: TappoToast(
                      color: Colors.green,
                      icon: Icon(Icons.check),
                      message: 'Đăng ký thành công, tự động đăng nhập',
                    ),
                    gravity: ToastGravity.BOTTOM,
                    toastDuration: Duration(seconds: 2),
                  );
                  _authenticationBloc..add(AuthenticationEventLoggedIn());
                  Navigator.pop(context);
                }
              },
              builder: (context, registerState) {
                return Background(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: size.height * 0.03),
                        Text(
                          "ĐĂNG KÝ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: size.height * 0.03),
                        SvgPicture.asset(
                          "assets/icons/signup.svg",
                          height: size.height * 0.13,
                        ),
                        RoundedInputField(
                          hintText: "Địa chỉ Email",
                          controller: _emailController,
                          icon: Icons.mail,
                          validatorText: registerState.isValidEmail
                              ? null
                              : 'Sai định dạng Email',
                        ), // Email Input
                        RoundedInputField(
                          hintText: "Username",
                          controller: _usernameController,
                          validatorText: registerState.isValidUsername
                              ? null
                              : 'Username ít nhất 6 kí tự, không gồm dấu cách',
                        ), // Email Input
                        RoundedInputField(
                          hintText: "Họ tên",
                          icon: Icons.verified_user,
                          controller: _fullnameController,
                        ), // Email Input
                        RoundedPasswordField(
                          controller: _passController,
                          validatorText: registerState.isValidPassword
                              ? null
                              : 'Mật khẩu ít nhất 6 ký tự, không gồm dấu cách',
                        ), // Password Input
                        RoundedPasswordField(
                          controller: _passConfirmController,
                          validatorText: registerState.isValidConfirmPassword
                              ? null
                              : 'Nhập lại mật khẩu không đúng',
                        ), // Password Input
                        RoundedButton(
                          text: "Đăng ký tài khoản",
                          press: isRegisterButtonEnable(registerState)
                              ? _onRegisterEmailAndPassword
                              : null,
                        ), // Register Button
                        SizedBox(height: size.height * 0.03),
                        AlreadyHaveAnAccountCheck(
                          login: false,
                          press: () {
                            Navigator.pop(context);
                          },
                        ),
                        // OrDivider(),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.center,
                        //   children: <Widget>[
                        //     SocalIcon(
                        //       iconSrc: "assets/icons/facebook.svg",
                        //       press: () {},
                        //     ),
                        //     SocalIcon(
                        //       iconSrc: "assets/icons/twitter.svg",
                        //       press: () {},
                        //     ),
                        //     SocalIcon(
                        //       iconSrc: "assets/icons/google-plus.svg",
                        //       press: () {},
                        //     ),
                        //   ],
                        // ) // Social Login button
                      ],
                    ),
                  ),
                );
              })),
    );
  }

  void _onRegisterEmailAndPassword() {
    _registerBloc.add(RegisterEventPressed(
      email: _emailController.text,
      password: _passController.text,
      confirmPassword: _passConfirmController.text,
      fullName: _fullnameController.text,
      username: _usernameController.text,
    ));
  }
}
