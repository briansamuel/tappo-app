import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tappo/blocs/profile/profile_bloc.dart';
import 'package:tappo/blocs/profile/profile_event.dart';
import 'package:tappo/blocs/profile/profile_state.dart';
import 'package:tappo/configs/api.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/repositories/user_repository.dart';

class ProfileScreen extends StatefulWidget {
  final User currentUser;
  ProfileScreen({this.currentUser});

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final Api _api = new Api();
  UserRepository _userRepository;
  ProfileBloc _profileBloc;
  User _currentUser;
  File _image;
  String _fileName;
  final picker = ImagePicker();
  List<int> imageBytes;
  String baseImage;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _profileBloc = BlocProvider.of<ProfileBloc>(context);
    _userRepository = UserRepository();
    _currentUser == widget.currentUser;
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(
        source: ImageSource.gallery, preferredCameraDevice: CameraDevice.rear);

    if (pickedFile != null) {
      _fileName = pickedFile.path;

      _profileBloc..add(ProfileEventChangeAvatar(image: File(pickedFile.path)));
    } else {
      print('No image selected.');
    }
  }

  Future saveProfile() async {
    _profileBloc
      ..add(
        ProfileEventPressSave(
          user: User(
            id: null,
            fullname: _fullNameController.text,
            address: _addressController.text,
            description: _descriptionController.text,
            avatarUrl: _fileName,
            socials: null,
          ),
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    final double circleRadius = 120.0;
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: saveProfile,
        icon: Icon(Icons.save),
        label: Text("Lưu lại"),
      ),
      body: SafeArea(
          child: BlocConsumer<ProfileBloc, ProfileState>(
        listener: (context, profileState) {
          if (profileState is ProfileStateSuccess) {
            _currentUser = profileState.user;
            _fullNameController.text = _currentUser.fullname;
            _descriptionController.text = _currentUser.description;
            _addressController.text = _currentUser.address;
            _image = profileState.image;
            if (!profileState.isInit && profileState.image == null) {
              Fluttertoast.showToast(
                  msg: "Cập nhật thông tin thành công",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.green,
                  textColor: Colors.white,
                  fontSize: 16.0);
            }
          } else if (profileState is ProfileStateFailed) {
            Fluttertoast.showToast(
                msg: "Có lại xảy ra, vui lòng thử lại",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          }
        },
        builder: (context, profileState) {
          if (profileState is ProfileStateSuccess) {
            _currentUser = profileState.user;
            _fullNameController.text = _currentUser.fullname;
            _descriptionController.text = _currentUser.description;
            _addressController.text = _currentUser.address;
            
            return Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Container(
                    width: circleRadius,
                    height: circleRadius,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 8.0,
                          offset: Offset(0.0, 5.0),
                        ),
                      ],
                    ),
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage: profileState.image != null
                          ? FileImage(profileState.image)
                          : _currentUser.avatarUrl != null
                              ? NetworkImage(
                                  _api.homeUrl + _currentUser.avatarUrl)
                              : AssetImage('assets/images/avatar-default.png'),
                      radius: 38.0,
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 18.0,
                          child: IconButton(
                              icon: Icon(
                                Icons.camera_alt,
                                size: 20.0,
                                color: Color(0xFF404040),
                              ),
                              onPressed: getImage),
                        ),
                      ),
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: _fullNameController,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.person),
                            labelText: 'Họ tên *',
                          ),
                          onSaved: (String value) {
                            // This optional block of code can be used to run
                            // code when the user saves the form.
                          },
                          validator: (String value) {
                            return (value != null && value.contains('@'))
                                ? 'Do not use the @ char.'
                                : null;
                          },
                        ),
                        TextFormField(
                          controller: _descriptionController,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.description),
                            labelText: 'Thông tin',
                          ),
                          onSaved: (String value) {
                            // This optional block of code can be used to run
                            // code when the user saves the form.
                          },
                          validator: (String value) {
                            return (value != null && value.contains('@'))
                                ? 'Do not use the @ char.'
                                : null;
                          },
                        ),
                        TextFormField(
                          controller: _addressController,
                          decoration: const InputDecoration(
                            icon: Icon(Icons.location_city),
                            labelText: 'Nơi ở *',
                          ),
                          onSaved: (String value) {
                            // This optional block of code can be used to run
                            // code when the user saves the form.
                          },
                          validator: (String value) {
                            return (value != null && value.contains('@'))
                                ? 'Do not use the @ char.'
                                : null;
                          },
                        ),

                        // Add TextFormFields and ElevatedButton here.
                      ],
                    ),
                  )
                ],
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      )),
    );
  }
}
