import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:tappo/blocs/home/home_bloc.dart';
import 'package:tappo/blocs/home/home_event.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/repositories/user_repository.dart';
import 'package:tappo/styles/constants.dart';
import 'package:tappo/views/home/home_screen.dart';
import 'package:tappo/views/profile/profile_screen.dart';
import 'package:tappo/views/setting/setting_screen.dart';

class MainScreen extends StatefulWidget {
  final User user;
  final UserRepository userRepository;
  const MainScreen({this.user, this.userRepository});
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  UserRepository _userRepository;
  PersistentTabController _controller;
  User _currentUser;
  HomeBloc _homeBloc;
  @override
  void initState() {
    super.initState();
    _currentUser = widget.user;
    _userRepository = widget.userRepository;
    _controller = PersistentTabController(initialIndex: 0);
    _homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  List<Widget> _buildScreens() {
    return [
      BlocProvider.value(
        value: _homeBloc,
        child: HomeScreen(
          currentUser: _currentUser,
          userRepository: _userRepository,
        ),
      ),
      SettingScreen(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.home),
        title: ("Trang cá nhân"),
        activeColorPrimary: mpPurple,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.settings),
        title: ("Cài đặt"),
        activeColorPrimary: mpPurple,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor: Colors.white, // Default is Colors.white.
      handleAndroidBackButtonPress: true, // Default is true.
      resizeToAvoidBottomInset:
          true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: true, // Default is true.
      hideNavigationBarWhenKeyboardShows:
          true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: Colors.white,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: ItemAnimationProperties(
        // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation(
        // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle:
          NavBarStyle.style1, // Choose the nav bar style with this property.
    );
  }
}
