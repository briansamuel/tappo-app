import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:tappo/blocs/home/home_bloc.dart';
import 'package:tappo/blocs/home/home_event.dart';
import 'package:tappo/blocs/home/home_state.dart';
import 'package:tappo/blocs/profile/profile_bloc.dart';
import 'package:tappo/blocs/profile/profile_event.dart';
import 'package:tappo/blocs/social/social_bloc.dart';
import 'package:tappo/blocs/social/social_event.dart';
import 'package:tappo/configs/api.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/repositories/user_repository.dart';
import 'package:tappo/styles/background.dart';
import 'package:tappo/styles/constants.dart';
import 'package:tappo/views/profile/profile_screen.dart';
import 'package:tappo/views/social/social_screen.dart';
import 'package:tappo/widgets/list_social_non_order_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeScreen extends StatefulWidget {
  final User currentUser;
  final UserRepository userRepository;
  HomeScreen({this.currentUser, this.userRepository});
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with AutomaticKeepAliveClientMixin {
  final Api _api = new Api();
  User _currentUser;
  UserRepository _userRepository;
  HomeBloc _homeBloc;
  ProfileBloc _profileBloc;
  SocialBloc _socialBloc;
  // Animation Float Button
  var renderOverlay = true;
  var visible = true;
  var switchLabelPosition = false;
  var extend = false;
  var rmicons = false;
  var customDialRoot = false;
  var closeManually = false;
  var useRAnimation = true;
  var isDialOpen = ValueNotifier<bool>(false);
  var speedDialDirection = SpeedDialDirection.Up;
  var selectedfABLocation = FloatingActionButtonLocation.endFloat;
  var items = [
    FloatingActionButtonLocation.startFloat,
    FloatingActionButtonLocation.startDocked,
    FloatingActionButtonLocation.centerFloat,
    FloatingActionButtonLocation.endFloat,
    FloatingActionButtonLocation.endDocked,
    FloatingActionButtonLocation.startTop,
    FloatingActionButtonLocation.centerTop,
    FloatingActionButtonLocation.endTop,
  ];
  List<Map> _listBackground = listBackground;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  void initState() {
    super.initState();
    // _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500))..addListener(() { setState(() {
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _profileBloc = BlocProvider.of<ProfileBloc>(context);
    _socialBloc = BlocProvider.of<SocialBloc>(context);
    _userRepository = widget.userRepository;
    // });});
    _currentUser = widget.currentUser;
  }

  // @override
  // void didUpdateWidget(covariant oldWidget) {
  //   // _homeBloc.add(HomeEventLoaded());
  // }

  void _onRefresh() async {
    _homeBloc.add(HomeEventRefresh());
    // if failed,use refreshFailed()
  }

  @override
  Widget build(BuildContext context) {
    final double circleRadius = 120.0;
    return WillPopScope(
      onWillPop: () async {
        if (isDialOpen.value) {
          isDialOpen.value = false;
          return false;
        }
        return true;
      },
      child: Scaffold(
        floatingActionButtonLocation: selectedfABLocation,
        floatingActionButton: SpeedDial(
          // animatedIcon: AnimatedIcons.menu_close,
          // animatedIconTheme: IconThemeData(size: 22.0),
          // / This is ignored if animatedIcon is non null
          // child: Text("open"),
          // activeChild: Text("close"),
          icon: Icons.add,

          activeIcon: Icons.close,
          spacing: 3,
          openCloseDial: isDialOpen,
          childPadding: EdgeInsets.all(5),
          spaceBetweenChildren: 4,
          dialRoot: customDialRoot
              ? (ctx, open, toggleChildren) {
                  return ElevatedButton(
                    onPressed: toggleChildren,
                    style: ElevatedButton.styleFrom(
                      primary: Colors.blue[900],
                      padding:
                          EdgeInsets.symmetric(horizontal: 22, vertical: 18),
                    ),
                    child: Text(
                      "Custom Dial Root",
                      style: TextStyle(fontSize: 17),
                    ),
                  );
                }
              : null,
          buttonSize: 56, // it's the SpeedDial size which defaults to 56 itself
          // iconTheme: IconThemeData(size: 22),
          label: extend ? Text("Open") : null, // The label of the main button.
          /// The active label of the main button, Defaults to label if not specified.
          activeLabel: extend ? Text("Close") : null,

          /// Transition Builder between label and activeLabel, defaults to FadeTransition.
          // labelTransitionBuilder: (widget, animation) => ScaleTransition(scale: animation,child: widget),
          /// The below button size defaults to 56 itself, its the SpeedDial childrens size
          childrenButtonSize: 56.0,
          visible: visible,
          direction: speedDialDirection,
          switchLabelPosition: switchLabelPosition,

          /// If true user is forced to close dial manually
          closeManually: closeManually,

          /// If false, backgroundOverlay will not be rendered.
          renderOverlay: renderOverlay,
          // overlayColor: Colors.black,
          // overlayOpacity: 0.5,
          onOpen: () => print('OPENING DIAL'),
          onClose: () => print('DIAL CLOSED'),
          useRotationAnimation: useRAnimation,
          tooltip: 'Open Speed Dial',
          heroTag: 'speed-dial-hero-tag',
          // foregroundColor: Colors.black,
          // backgroundColor: Colors.white,
          // activeForegroundColor: Colors.red,
          // activeBackgroundColor: Colors.blue,
          elevation: 8.0,
          isOpenOnStart: false,
          animationSpeed: 200,
          shape: customDialRoot ? RoundedRectangleBorder() : StadiumBorder(),
          // childMargin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          children: [
            SpeedDialChild(
              child: !rmicons ? Icon(Icons.accessibility) : null,
              backgroundColor: Colors.red,
              foregroundColor: Colors.white,
              label: 'Sửa thông tin',
              onTap: () {
                pushNewScreen(
                  context,
                  screen: BlocProvider.value(
                    value: _profileBloc..add(ProfileEventInitial()),
                    child: ProfileScreen(),
                  ),
                  withNavBar: false, // OPTIONAL VALUE. True by default.
                  pageTransitionAnimation: PageTransitionAnimation.cupertino,
                ).then((value) {
                  // Close Profile Screen
                  _homeBloc..add(HomeEventLoaded());
                });
              },
            ),
            SpeedDialChild(
              child: !rmicons ? Icon(Icons.brush) : null,
              backgroundColor: Colors.deepOrange,
              foregroundColor: Colors.white,
              label: 'Sửa Social',
              onTap: () {
                pushNewScreen(
                  context,
                  screen: BlocProvider.value(
                    value: _socialBloc..add(SocialEventInitial()),
                    child: SocialScreen(),
                  ),
                  withNavBar: false, // OPTIONAL VALUE. True by default.
                  pageTransitionAnimation: PageTransitionAnimation.cupertino,
                ).then((value) {
                  // Close Social Screen
                  _homeBloc..add(HomeEventLoaded());
                });
              },
            ),
          ],
        ),
        body: SmartRefresher(
            enablePullDown: true,
            header: WaterDropMaterialHeader(
              backgroundColor: mpPurple,
            ),
            controller: _refreshController,
            onRefresh: _onRefresh,
            child: SafeArea(
              child: BlocConsumer<HomeBloc, HomeState>(
                  listener: (context, homeState) {
                if (homeState is HomeStateSuccess && homeState.isRefresh) {
                  _refreshController.refreshCompleted();
                } else if (homeState is HomeStateFailed) {
                  _refreshController.refreshFailed();
                }
              }, builder: (context, homeState) {
                if (homeState is HomeStateSuccess) {
                  _currentUser = homeState.user;
                  return SingleChildScrollView(
                      child: Container(
                    width: double.infinity,
                    decoration: _listBackground[_currentUser.background]
                                ['colors'] !=
                            null
                        ? BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: _listBackground[_currentUser.background]
                                  ['colors'],
                            ),
                          )
                        : BoxDecoration(
                            color: _listBackground[_currentUser.background]
                                ['color'],
                            borderRadius:
                                new BorderRadius.all(Radius.circular(10.0))),
                    child: Stack(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Stack(
                          alignment: Alignment.topCenter,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                top: circleRadius / 2.0,
                              ),

                              ///here we create space for the circle avatar to get ut of the box
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      blurRadius: 8.0,
                                      offset: Offset(0.0, 5.0),
                                    ),
                                  ],
                                ),
                                width: double.infinity,
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 15.0, bottom: 15.0),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(
                                          height: circleRadius / 2,
                                        ),
                                        Text(
                                          _currentUser.fullname ?? '',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 34.0),
                                        ),
                                        Text(
                                          _currentUser.address ?? '',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16.0,
                                              color: Colors.lightBlueAccent),
                                        ),
                                        SizedBox(
                                          height: 30.0,
                                        ),
                                        // List Social
                                        ListSocialNonOrderWidget(
                                          socials: _currentUser.socials,
                                        )
                                      ],
                                    )),
                              ),
                            ),

                            ///Image Avatar
                            Container(
                              width: circleRadius,
                              height: circleRadius,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 8.0,
                                    offset: Offset(0.0, 5.0),
                                  ),
                                ],
                              ),
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                backgroundImage: _currentUser.avatarUrl != null
                                    ? NetworkImage(
                                        _api.homeUrl + _currentUser.avatarUrl)
                                    : AssetImage(
                                        'assets/images/avatar-default.png'),
                                radius: 38.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ]),
                  ));
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              }),
            )),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
