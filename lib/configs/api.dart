class Api {
  String homeUrl = 'https://tappo.xyz/';
  String baseUrl = 'https://tappo.xyz/api/v1/';
  String loginUrl = 'auth/login';
  String registerUrl = 'auth/register';
  String userUrl = 'me';
  String updateProfileUrl = 'updateProfile';
  String changePassword = 'changePassword';
  String changeBackground = 'changeBackground';
}
