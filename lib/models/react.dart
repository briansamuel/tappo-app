import 'package:equatable/equatable.dart';
import 'package:tappo/models/user.dart';

enum TypeReact {
  Like,
  Love,
  Heart,
  Angry,
}

class React extends Equatable {
  final int id;
  final User user;
  final TypeReact type;

  React(this.id, this.user, this.type);

  @override
  List<Object> get props => [id, user, type];
}
