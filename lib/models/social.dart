import 'dart:convert';

class Social {
  final String label;
  final String content;
  final String icon;

  Social({this.label, this.content, this.icon});

  factory Social.fromJson(Map<String, dynamic> json) {
    return Social(
      label: json['label'],
      content: json['content'],
      icon: json['icon'],
    );
  }

  static Map<String, dynamic> toJson(Social social) => {
        'label': social.label,
        'content': social.content,
        'icon': social.icon,
      };

  static List<Social> listFromJson(jsonSocial) {
    List<Social> listSocial = [];
    final listJson = json.decode(jsonSocial) as List;
    listJson.forEach((element) {
      if (element is String) {
        element = json.decode(element);
      }
      listSocial.add(Social.fromJson(element));
    });
    return listSocial;
  }

  static String listToString(List<Social> social) {
    List<Map<String, dynamic>> listSocial = [];
    if (social == null) {
      final listString = json.encode(listSocial);
      return listString;
    }
    social.forEach((element) {
      listSocial.add(toJson(element));
    });
    final listString = json.encode(listSocial);
    return listString;
  }
}
