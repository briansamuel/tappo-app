import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
class Location extends Equatable {
  String latitude;
  String longitude;
  String address;

  Location({
    @required this.latitude,
    @required this.longitude,
    @required this.address,
  });

  @override
  List<Object> get props => [latitude, longitude, address];
}
