import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:tappo/models/social.dart';

@immutable
// ignore: must_be_immutable
class User extends Equatable {
  final int id;
  final String fullname;
  final String description;
  final String address;
  final String avatarUrl;
  final int background;
  final String qrCode;
  List<Social> socials;
  User({
    @required this.id,
    @required this.fullname,
    @required this.description,
    @required this.address,
    @required this.avatarUrl,
    this.qrCode,
    this.background,
    this.socials,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      fullname: json['full_name'],
      description: json['description'],
      address: json['address'],
      avatarUrl: json['avatar'],
      qrCode: json['qr_code'],
      background: json['background'],
      socials: json['list_social'] != null
          ? Social.listFromJson(json['list_social'])
          : null,
    );
  }

  User copyWith({
    int id,
    String fullname,
    String description,
    String address,
    String avatarUrl,
    String qrCode,
    int background,
    List<Social> socials,
  }) {
    return User(
      id: id ?? this.id,
      fullname: fullname ?? this.fullname,
      description: description ?? this.description,
      address: address ?? this.address,
      avatarUrl: avatarUrl ?? this.avatarUrl,
      qrCode: qrCode ?? this.qrCode,
      background: background ?? this.background,
      socials: socials ?? this.socials,
    );
  }

  User fromJson(Map<String, dynamic> json) {
    return copyWith(
      id: json['id'],
      fullname: json['full_name'],
      description: json['full_name'],
      address: json['address'],
      avatarUrl: json['avatar'],
      qrCode: json['qr_code'],
      background: json['background'],
      socials: Social.listFromJson(json['list_social']),
    );
  }

  @override
  List<Object> get props =>
      [id, fullname, description, address, avatarUrl, qrCode, socials];
}
