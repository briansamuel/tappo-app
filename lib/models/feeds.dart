import 'package:equatable/equatable.dart';
import 'package:tappo/models/comment.dart';
import 'package:tappo/models/react.dart';

import 'user.dart';

class Feeds extends Equatable {
  final int id;
  final User user;
  final DateTime createdAt;
  final String status;
  final List<ImageFeed> imageFeed;
  final List<React> listReact;
  final List<Comment> comments;
  final bool isYourReact;

  const Feeds(
      {this.id,
      this.user,
      this.createdAt,
      this.status,
      this.imageFeed,
      this.listReact,
      this.comments,
      this.isYourReact});

  @override
  List<Object> get props => [
        id,
        user,
        createdAt,
        status,
        imageFeed,
        listReact,
        comments,
        isYourReact
      ];

  static Feeds fromJson(json) {
    return Feeds(
      id: json['id'],
      imageFeed: ImageFeed.listFromJson(
        json['feed_photos'],
      ),
    );
  }
}

class ImageFeed extends Equatable {
  final int id;
  final User user;
  final DateTime createdAt;
  final String status;
  final String imageUrl;
  final List<React> listReact;
  final List<Comment> comments;
  final bool isYourReact;

  ImageFeed(
      {this.id,
      this.user,
      this.createdAt,
      this.status,
      this.imageUrl,
      this.listReact,
      this.comments,
      this.isYourReact});

  static ImageFeed fromJson(json) {
    return ImageFeed(
      id: json['id'],
      status: json['status'],
      imageUrl: json['photo_url'],
    );
  }

  static List<ImageFeed> listFromJson(json) {
    List<ImageFeed> listImage = [];
    final listJson = json as List;
    listJson.forEach((element) {
      listImage.add(ImageFeed.fromJson(element));
    });
    return listImage;
  }

  @override
  List<Object> get props =>
      [id, user, createdAt, status, imageUrl, listReact, comments, isYourReact];
}
