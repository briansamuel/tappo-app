import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:tappo/blocs/profile/profile_event.dart';
import 'package:tappo/blocs/profile/profile_state.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/repositories/user_repository.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final UserRepository _userRepository;
  User _currentUser;
  ProfileBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(ProfileStateInitial());

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    try {
      if (event is ProfileEventInitial) {
        _currentUser = _userRepository.currentUser;
        if (_currentUser != null) {
          yield ProfileStateSuccess(user: _currentUser);
        } else {
          yield ProfileStateFailed();
        }
      } else if (event is ProfileEventChangeAvatar) {
        yield ProfileStateLoading();
        if (_currentUser != null) {
          yield ProfileStateSuccess(user: _currentUser, image: event.image);
        } else {
          yield ProfileStateFailed();
        }
      } else if (event is ProfileEventPressSave) {
        yield ProfileStateLoading();
        final bool response = await _userRepository.updateProfile(event.user);
        if (response) {
          _currentUser = _userRepository.currentUser;
          yield ProfileStateSuccess(user: _currentUser);
        } else {
          yield ProfileStateFailed();
        }
      } else if (event is ProfileEventChangeBackground) {
        yield ProfileStateLoading();
        final bool response =
            await _userRepository.updateBackground(event.background);
        if (response) {
          _currentUser = _userRepository.currentUser;
          yield ProfileStateSuccess(user: _currentUser);
        } else {
          yield ProfileStateFailed();
        }
      }
    } catch (e) {
      throw (e);
    }
  }
}
