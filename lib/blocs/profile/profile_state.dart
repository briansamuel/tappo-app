import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:tappo/models/user.dart';

abstract class ProfileState extends Equatable {
  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class ProfileStateInitial extends ProfileState {
  User user;
  // ignore: avoid_init_to_null
  ProfileStateInitial({this.user});
  @override
  List<Object> get props => [user];
}

class ProfileStateLoading extends ProfileState {}

// ignore: must_be_immutable
class ProfileStateSuccess extends ProfileState {
  User user;
  // ignore: avoid_init_to_null
  File image = null;
  bool isInit;
  ProfileStateSuccess({this.user, this.image, this.isInit = false});
  @override
  List<Object> get props => [user, image];
}

class ProfileStateFailed extends ProfileState {}
