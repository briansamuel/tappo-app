import 'dart:async';
import 'dart:developer' as developer;
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tappo/models/user.dart';

@immutable
abstract class ProfileEvent extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ProfileEventInitial extends ProfileEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ProfileEventLoading extends ProfileEvent {}

class ProfileEventPressSave extends ProfileEvent {
  final User user;

  ProfileEventPressSave({this.user});
  List<Object> get props => [user];
}

class ProfileEventChangeAvatar extends ProfileEvent {
  final File image;

  ProfileEventChangeAvatar({this.image});
  @override
  List<Object> get props => [image];
}

class ProfileEventChangeBackground extends ProfileEvent {
  final int background;

  ProfileEventChangeBackground({this.background});
  @override
  List<Object> get props => [background];
}
