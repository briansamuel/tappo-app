import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
  @override
  List<Object> get props => [];
}

class RegisterEventInitial extends RegisterEvent {}

// Sự kiện thay đổi text email input
class RegisterEventEmailChanged extends RegisterEvent {
  final String email;
  //contructor
  const RegisterEventEmailChanged({this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'Email changed: $email';
}

// Sự kiện thay đổi text password input
class RegisterEventPasswordChanged extends RegisterEvent {
  final String password;
  final String confirmPassword;
  //contructor
  const RegisterEventPasswordChanged({this.password, this.confirmPassword});

  @override
  List<Object> get props => [password, confirmPassword];

  @override
  String toString() => 'Password changed: $password';
}

class RegisterEventConfirmPasswordChanged extends RegisterEvent {
  final String password;
  final String confirmPassword;
  //contructor
  const RegisterEventConfirmPasswordChanged(
      {this.password, this.confirmPassword});

  @override
  List<Object> get props => [password, confirmPassword];

  @override
  String toString() => 'Password changed: $password';
}

class RegisterEventUsernameChanged extends RegisterEvent {
  final String username;
  //contructor
  const RegisterEventUsernameChanged({this.username});

  @override
  List<Object> get props => [username];

  @override
  String toString() => 'Password changed: $username';
}

// Sự kiện khi click đăng ký
class RegisterEventPressed extends RegisterEvent {
  final String email;
  final String username;
  final String password;
  final String confirmPassword;
  final String fullName;
  RegisterEventPressed(
      {this.email,
      this.username,
      this.password,
      this.confirmPassword,
      this.fullName});

  @override
  List<Object> get props =>
      [email, username, password, confirmPassword, fullName];

  @override
  String toString() =>
      'RegisterEventPressed email: $email, password: $password';
}
