import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';

import 'package:rxdart/rxdart.dart';
import 'package:tappo/blocs/register/register_event.dart';
import 'package:tappo/blocs/register/register_state.dart';
import 'package:tappo/repositories/auth_repository.dart';
import 'package:tappo/repositories/user_repository.dart';
import 'package:tappo/validators/validtors.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AuthRepository _authRepository;
  // Giới hạn thời gian 2 lần nhấn button final AuthRepository _authRepository;
  RegisterBloc({@required AuthRepository authRepository})
      : assert(authRepository != null),
        _authRepository = authRepository,
        super(RegisterState.initial());
  @override
  Stream<Transition<RegisterEvent, RegisterState>> transformEvents(
      Stream<RegisterEvent> registerEvent,
      TransitionFunction<RegisterEvent, RegisterState> transitionFn) {
    // TODO: implement transformEvents
    // Những sự kiện cần delay thời gian nhấn

    final debounceStream = registerEvent.where((registerEvent) {
      return (registerEvent is RegisterEventEmailChanged ||
          registerEvent is RegisterEventPasswordChanged ||
          registerEvent is RegisterEventUsernameChanged ||
          registerEvent is RegisterEventConfirmPasswordChanged);
    }).debounceTime(const Duration(
        milliseconds: 300)); // Giới hạn 3s cho sự kiện - yêu cầu rxdart

    // Những sự kiện không cần delay thời gian nhấn
    final nonDebounceStream = registerEvent.where((registerEvent) {
      return (registerEvent is RegisterEventPressed);
    });

    // Merge 2 luồng với nhau - Rxdart
    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent registerEvent) async* {
    // TODO: implement mapEventToState
    final registerState = state;
    if (registerEvent is RegisterEventInitial) {
      // Sự kiện thay đổi Mail
      yield RegisterState.initial();
    } else if (registerEvent is RegisterEventEmailChanged) {
      // Sự kiện thay đổi Mail
      yield registerState.cloneAndUpdate(
          isValidEmail: Validators.isValidEmail(registerEvent.email));
    } else if (registerEvent is RegisterEventPasswordChanged) {
      // Sự kiện thay đổi Pass
      yield registerState.cloneAndUpdate(
          isValidPassword: Validators.isValidPassword(registerEvent.password),
          isValidConfirmPassword: Validators.isMatchPasswordConfirm(
              registerEvent.password, registerEvent.confirmPassword));
    } else if (registerEvent is RegisterEventConfirmPasswordChanged) {
      yield registerState.cloneAndUpdate(
          isValidConfirmPassword: Validators.isMatchPasswordConfirm(
              registerEvent.password, registerEvent.confirmPassword));
    } else if (registerEvent is RegisterEventUsernameChanged) {
      // Sự kiện thay đổi Username
      yield registerState.cloneAndUpdate(
        isValidUsername: Validators.isValidUsername(registerEvent.username),
      );
    } else if (registerEvent is RegisterEventPressed) {
      // Sự kiện button nhấn đăng ký
       yield RegisterState.loading();
      try {
        final result = await _authRepository.createUserWithEmailAndPassword(
            email: registerEvent.email,
            username: registerEvent.username,
            password: registerEvent.password,
            confirmPassword: registerEvent.confirmPassword,
            fullName: registerEvent.fullName);
        if (result) {
          yield RegisterState.success();
        } else {
          yield RegisterState.failure('Đăng ký thất bại, vui lòng thử lại sau');
        }
      } catch (e) {
        yield RegisterState.failure(e.message.toString());
      }
    }
  }
}
