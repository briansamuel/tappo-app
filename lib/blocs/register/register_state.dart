import 'package:flutter/cupertino.dart';

@immutable
class RegisterState {
  final bool isValidEmail;
  final bool isValidUsername;
  final bool isValidPassword;
  final bool isValidConfirmPassword;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  String message;
  bool get isValidEmailAndPassword =>
      isValidEmail &&
      isValidPassword &&
      isValidConfirmPassword &&
      isValidConfirmPassword;
  RegisterState({
    @required this.isValidEmail,
    @required this.isValidUsername,
    @required this.isValidPassword,
    @required this.isValidConfirmPassword,
    @required this.isSubmitting,
    @required this.isSuccess,
    @required this.isFailure,
    this.message,
  });

  // Mỗi state là 1 object, hoặc static object
  // Khởi tạo các state bằng phương thức static/method
  factory RegisterState.initial() {
    return RegisterState(
      isValidEmail: true,
      isValidUsername: true,
      isValidPassword: true,
      isValidConfirmPassword: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }
  // Loading state - trạng thái đang submit form
  factory RegisterState.loading() {
    return RegisterState(
      isValidEmail: true,
      isValidUsername: true,
      isValidPassword: true,
      isValidConfirmPassword: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }

  // Success state - trạng thái đăng nhập thành công
  factory RegisterState.success() {
    return RegisterState(
      isValidEmail: true,
      isValidUsername: true,
      isValidPassword: true,
      isValidConfirmPassword: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
    );
  }

  // Failure state - trạng thái đăng nhập lỗi
  factory RegisterState.failure(message) {
    return RegisterState(
      isValidEmail: true,
      isValidUsername: true,
      isValidPassword: true,
      isValidConfirmPassword: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
      message: message,
    );
  }

  // Clone state
  RegisterState cloneWith({
    bool isValidEmail,
    bool isValidUsername,
    bool isValidPassword,
    bool isValidConfirmPassword,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
  }) {
    return RegisterState(
      isValidEmail: isValidEmail ?? this.isValidEmail,
      isValidUsername: isValidUsername ?? this.isValidUsername,
      isValidPassword: isValidPassword ?? this.isValidPassword,
      isValidConfirmPassword:
          isValidConfirmPassword ?? this.isValidConfirmPassword,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
    );
  }

  // Clone state and update
  RegisterState cloneAndUpdate({
    bool isValidEmail,
    bool isValidUsername,
    bool isValidPassword,
    bool isValidConfirmPassword,
  }) {
    return cloneWith(
      isValidEmail: isValidEmail ?? this.isValidEmail,
      isValidUsername: isValidUsername ?? this.isValidUsername,
      isValidPassword: isValidPassword ?? this.isValidPassword,
      isValidConfirmPassword:
          isValidConfirmPassword ?? this.isValidConfirmPassword,
      isFailure: false,
      isSubmitting: false,
      isSuccess: false,
    );
  }
}
