import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tappo/blocs/login/login_event.dart';
import 'package:tappo/blocs/login/login_state.dart';
import 'package:tappo/repositories/auth_repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tappo/validators/validtors.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository _authRepository;
  LoginBloc({@required AuthRepository authRepository})
      : assert(authRepository != null),
        _authRepository = authRepository,
        super(LoginState.initial());

  // Giới hạn thời gian 2 lần nhấn button
  @override
  Stream<Transition<LoginEvent, LoginState>> transformEvents(
      Stream<LoginEvent> loginEvent,
      TransitionFunction<LoginEvent, LoginState> transitionFn) {
    // TODO: implement transformEvents
    // Những sự kiện cần delay thời gian nhấn

    final debounceStream = loginEvent.where((loginEvent) {
      return (loginEvent is LoginEventEmailChanged ||
          loginEvent is LoginEventPasswordChanged);
    }).debounceTime(const Duration(
        milliseconds: 500)); // Giới hạn 3s cho sự kiện - yêu cầu rxdart

    // Những sự kiện không cần delay thời gian nhấn
    final nonDebounceStream = loginEvent.where((loginEvent) {
      return (loginEvent is LoginEventWithEmailAndPasswordPressed ||
          loginEvent is LoginEventWithGooglePressed);
    });

    // Merge 2 luồng với nhau - Rxdart
    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent loginEvent) async* {
    final loginState = state;
    if (loginEvent is LoginEventInitial) {
      yield LoginState.initial();
    } else if (loginEvent is LoginEventEmailChanged) {
      yield state.cloneAndUpdate(
          isValidEmail: Validators.isValidEmail(loginEvent.email));
    } else if (loginEvent is LoginEventPasswordChanged) {
      yield state.cloneAndUpdate(
          isValidPassword: Validators.isValidPassword(loginEvent.password));
    } else if (loginEvent is LoginEventWithGooglePressed) {
      try {
        // await _userRepository.signInWithGoole();
        yield LoginState.success();
      } catch (_) {
        yield LoginState.failure('Đăng nhập thất bại');
      }
    } else if (loginEvent is LoginEventWithEmailAndPasswordPressed) {
      try {
        yield LoginState.loading();
        await Future.delayed(Duration(seconds: 1));
        await _authRepository.signInWithEmailAndPassword(
            loginEvent.email, loginEvent.password);
        yield LoginState.success();
      } catch (e) {
        yield LoginState.failure('Lỗi đăng nhập, vui lòng thử lại');
      }
    }
  }
}
