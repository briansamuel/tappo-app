import 'package:equatable/equatable.dart';
import 'package:tappo/models/user.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();

  @override
  List<Object> get props => [];
}

class AuthenticationStateInitial extends AuthenticationState {}

class AuthenticationStateFailure extends AuthenticationState {
  final String message;
  const AuthenticationStateFailure({this.message});

  @override
  List<Object> get props => [message];
}

class AuthenticationStateSuccess extends AuthenticationState {
  final User user;
  const AuthenticationStateSuccess({this.user});

  @override
  List<Object> get props => [user];

  @override
  String toString() => 'AuthenticationStateSuccess, email: $user';
}
