import 'package:equatable/equatable.dart';

abstract class AuthenticationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthenticationEventStarted extends AuthenticationEvent {
} // sự kiện gọi khi khởi động app

class AuthenticationEventLoggedIn extends AuthenticationEvent {
} // sự kiện gọi khi đăng nhập

class AuthenticationEventLoggedOut extends AuthenticationEvent {
} // sự kiện gọi khi đăng xuất
