import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tappo/repositories/auth_repository.dart';
import 'package:tappo/repositories/user_repository.dart';
import './authentication_event.dart';
import './authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthRepository _authRepository;
  final UserRepository _userRepository;

  AuthenticationBloc(
      {@required AuthRepository authRepository,
      @required UserRepository userRepository})
      : assert(userRepository != null),
        _authRepository = authRepository,
        _userRepository = userRepository,
        super(AuthenticationStateInitial());
  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent authenticationEvent) async* {
    try {
      if (authenticationEvent is AuthenticationEventStarted) {
        final isSignedIn = await _authRepository.isSignedIn();
        if (isSignedIn) {
          final currentUser = await _userRepository.getUser();
          yield AuthenticationStateSuccess(user: currentUser);
        } else {
          yield AuthenticationStateFailure(message: 'Chưa đăng nhập');
        }
      } else if (authenticationEvent is AuthenticationEventLoggedIn) {
        final currentUser = await _userRepository.getUser();
        yield AuthenticationStateSuccess(user: currentUser);
      } else if (authenticationEvent is AuthenticationEventLoggedOut) {
        _authRepository.logout();
        yield AuthenticationStateFailure(message: 'Đăng xuất thành công');
      } else {
        yield AuthenticationStateFailure(
            message: 'Lỗi đăng xuất, vui lòng khởi động lại ứng dụng');
      }
    } catch (exception) {
      
      yield AuthenticationStateFailure(
          message: 'Lỗi phiên, vui lòng khởi động lại ứng dụng');
    }
  }
}
