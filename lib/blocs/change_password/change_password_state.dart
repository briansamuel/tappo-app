import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:tappo/models/user.dart';

abstract class ChangePasswordState extends Equatable {
  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class ChangePasswordStateInitial extends ChangePasswordState {}

class ChangePasswordStateLoading extends ChangePasswordState {}

// ignore: must_be_immutable
class ChangePasswordStateSuccess extends ChangePasswordState {}

class ChangePasswordStateShowHide extends ChangePasswordState {
  final bool isShowHidePassword;
  ChangePasswordStateShowHide({this.isShowHidePassword});

  @override
  List<Object> get props => [isShowHidePassword];
}

class ChangePasswordStateInputChange extends ChangePasswordState {
  final bool validPassword;
  final bool validNewPassword;
  final bool validNewPasswordConfirm;
  final String message;
  ChangePasswordStateInputChange({
    this.validPassword,
    this.validNewPassword,
    this.validNewPasswordConfirm,
    this.message,
  });

  @override
  List<Object> get props =>
      [validPassword, validNewPassword, validNewPasswordConfirm, message];
}

class ChangePasswordStateFailed extends ChangePasswordState {
  final String message;
  ChangePasswordStateFailed({this.message});

  @override
  List<Object> get props => [message];
}
