import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

import 'package:tappo/blocs/change_password/change_password_event.dart';
import 'package:tappo/blocs/change_password/change_password_state.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/repositories/user_repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tappo/validators/validtors.dart';

class ChangePasswordBloc
    extends Bloc<ChangePasswordEvent, ChangePasswordState> {
  final UserRepository _userRepository;
  User _currentUser;
  ChangePasswordBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(ChangePasswordStateInitial());

  // Giới hạn thời gian 2 lần gõ phím
  @override
  Stream<Transition<ChangePasswordEvent, ChangePasswordState>> transformEvents(
      Stream<ChangePasswordEvent> loginEvent,
      TransitionFunction<ChangePasswordEvent, ChangePasswordState>
          transitionFn) {
    // TODO: implement transformEvents
    // Những sự kiện cần delay thời gian nhấn

    final debounceStream = loginEvent.where((loginEvent) {
      return (loginEvent is ChangePasswordEventInputChange);
    }).debounceTime(const Duration(
        milliseconds: 300)); // Giới hạn 3s cho sự kiện - yêu cầu rxdart

    // Những sự kiện không cần delay thời gian nhấn
    final nonDebounceStream = loginEvent.where((loginEvent) {
      return (loginEvent is ChangePasswordEventPressSave ||
          loginEvent is ChangePasswordEventInitial);
    });

    // Merge 2 luồng với nhau - Rxdart
    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<ChangePasswordState> mapEventToState(
    ChangePasswordEvent event,
  ) async* {
    try {
      final changePasswordState = state;
      if (event is ChangePasswordEventInitial) {
      } else if (event is ChangePasswordEventPressSave) {
        yield ChangePasswordStateLoading();
        final bool response =
            await _userRepository.updatePassword(event.dataPass);
        if (response) {
          yield ChangePasswordStateSuccess();
        } else {
          yield ChangePasswordStateFailed(
              message: 'Cập nhật mật khẩu thất bại');
        }
      } else if (event is ChangePasswordEventInputChange) {
        yield ChangePasswordStateInputChange(
            validPassword:
                Validators.isValidPassword(event.dataPass['oldPassword']),
            validNewPassword:
                Validators.isValidPassword(event.dataPass['newPassword']),
            validNewPasswordConfirm: Validators.isMatchPasswordConfirm(
                event.dataPass['newPassword'],
                event.dataPass['newPasswordComfirm']));
      }
    } catch (e) {
      yield ChangePasswordStateFailed(message: e.message);
    }
  }
}
