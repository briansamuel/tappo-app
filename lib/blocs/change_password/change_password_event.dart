import 'dart:async';
import 'dart:developer' as developer;
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tappo/models/user.dart';

@immutable
abstract class ChangePasswordEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ChangePasswordEventInitial extends ChangePasswordEvent {
  @override
  List<Object> get props => [];
}

class ChangePasswordEventLoading extends ChangePasswordEvent {}

class ChangePasswordEventInputChange extends ChangePasswordEvent {
  final Map<String, String> dataPass;
  ChangePasswordEventInputChange({this.dataPass});
  List<Object> get props => [dataPass];
}

class ChangePasswordEventPressSave extends ChangePasswordEvent {
  final Map<String, String> dataPass;
  ChangePasswordEventPressSave({this.dataPass});
  List<Object> get props => [dataPass];
}
