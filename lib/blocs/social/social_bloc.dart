import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

import 'package:tappo/blocs/social/social_event.dart';
import 'package:tappo/blocs/social/social_state.dart';
import 'package:tappo/models/user.dart';
import 'package:tappo/repositories/user_repository.dart';

class SocialBloc extends Bloc<SocialEvent, SocialState> {
  final UserRepository _userRepository;
  User _currentUser;

  SocialBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(SocialStateInitial());

  @override
  Future<void> close() async {
    // dispose objects
    await super.close();
  }

  @override
  Stream<SocialState> mapEventToState(
    SocialEvent event,
  ) async* {
    try {
      if (event is SocialEventInitial) {
        _currentUser = await _userRepository.getUserStorage();
        if (_currentUser.socials != null) {
          yield SocialStateInitial(socials: _currentUser.socials);
        } else {
          yield SocialStateFailure(socials: _currentUser.socials);
        }
      } else if (event is SocialEventLoad) {
        yield SocialStateloading();
        if (_currentUser.socials != null) {
          yield SocialStateSuccess(socials: _currentUser.socials);
        } else {
          yield SocialStateFailure();
        }
      } else if (event is SocialEventPressSave) {
        yield SocialStateloading();
        final result = await _userRepository.updateSocial(event.socials);
        if (result) {
          yield SocialStateSuccess(socials: _currentUser.socials);
        } else {
          yield SocialStateFailure();
        }
      } else if (event is SocialEventAddSocial) {
        yield SocialStateloading();
        _currentUser.socials.insert(0, event.social);
        final _socials = _currentUser.socials;
        yield SocialStateSuccess(socials: _socials, isAddSocial: true);
      } else if (event is SocialEventEditSocial) {
        yield SocialStateloading();
        _currentUser.socials[event.index] = event.social;
        final _socials = _currentUser.socials;
        yield SocialStateSuccess(socials: _socials, isEditSocial: true);
      } else if (event is SocialEventRemoveSocial) {
        yield SocialStateloading();
        _currentUser.socials = event.socials;
        final _socials = event.socials;
        yield SocialStateSuccess(socials: _socials);
      }
    } catch (e) {
      yield SocialStateFailure(socials: _currentUser.socials);
    }
  }
}
