import 'dart:async';

import 'package:equatable/equatable.dart';

import 'package:meta/meta.dart';
import 'package:tappo/models/social.dart';

@immutable
abstract class SocialEvent extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SocialEventInitial extends SocialEvent {}

class SocialEventLoad extends SocialEvent {}

class SocialEventPressSave extends SocialEvent {
  final List<Social> socials;

  SocialEventPressSave({this.socials});

  @override
  List<Object> get props => [socials];
}

class SocialEventAddSocial extends SocialEvent {
  final Social social;

  SocialEventAddSocial({this.social});

  @override
  List<Object> get props => [social];
}

class SocialEventEditSocial extends SocialEvent {
  final Social social;
  final int index;
  SocialEventEditSocial({this.social, this.index});

  @override
  List<Object> get props => [social, index];
}

class SocialEventRemoveSocial extends SocialEvent {
  final List<Social> socials;

  SocialEventRemoveSocial({this.socials});

  @override
  List<Object> get props => [socials];
}
