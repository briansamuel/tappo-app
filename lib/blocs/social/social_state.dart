import 'package:equatable/equatable.dart';
import 'package:tappo/models/social.dart';

abstract class SocialState extends Equatable {
  /// notify change state without deep clone state

  SocialState();

  @override
  List<Object> get props => [];
}

class SocialStateInitial extends SocialState {
  final List<Social> socials;
  SocialStateInitial({
    this.socials,
  });

  @override
  List<Object> get props => [socials];
}

class SocialStateloading extends SocialState {}

class SocialStateSuccess extends SocialState {
  final List<Social> socials;
  final bool isAddSocial;
  final bool isEditSocial;
  SocialStateSuccess(
      {this.socials, this.isAddSocial = false, this.isEditSocial = false});

  @override
  // TODO: implement props
  List<Object> get props => [socials, isAddSocial, isEditSocial];
}

class SocialStateFailure extends SocialState {
  final List<Social> socials;

  SocialStateFailure({this.socials});

  @override
  List<Object> get props => [socials];
}
