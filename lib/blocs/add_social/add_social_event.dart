

import 'package:equatable/equatable.dart';

import 'package:meta/meta.dart';

@immutable
abstract class AddSocialEvent extends Equatable {}

class AddSocialEventSave extends AddSocialEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}
