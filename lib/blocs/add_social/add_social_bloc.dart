import 'dart:async';


import 'package:bloc/bloc.dart';
import 'package:tappo/blocs/add_social/add_social_event.dart';
import 'package:tappo/blocs/add_social/add_social_state.dart';

class AddSocialBloc extends Bloc<AddSocialEvent, AddSocialState> {
  AddSocialBloc() : super(AddSocialStateInitial());

  // todo: check singleton for logic in project
  // use GetIt for DI in projct

  @override
  Future<void> close() async {
    // dispose objects
    await super.close();
  }

  @override
  Stream<AddSocialState> mapEventToState(
    AddSocialEvent event,
  ) async* {
    try {} catch (e) {
      yield state;
    }
  }
}
