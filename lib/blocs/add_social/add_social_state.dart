import 'package:equatable/equatable.dart';

abstract class AddSocialState extends Equatable {
  /// notify change state without deep clone state

  @override
  List<Object> get props => [];
}

class AddSocialStateInitial extends AddSocialState {}

class AddSocialStateLoading extends AddSocialState {}

class AddSocialStateSuccess extends AddSocialState {}

class AddSocialStateFailure extends AddSocialState {}
