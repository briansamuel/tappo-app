import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class HomeEventInitial extends HomeEvent {}

class HomeEventLoading extends HomeEvent {}

class HomeEventLoaded extends HomeEvent {}

class HomeEventRefresh extends HomeEvent {}

class HomeEventFailure extends HomeEvent {}
