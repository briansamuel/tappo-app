import 'package:equatable/equatable.dart';
import 'package:tappo/models/feeds.dart';
import 'package:tappo/models/story.dart';
import 'package:tappo/models/user.dart';

abstract class HomeState extends Equatable {
  @override
  List<Object> get props => [];
}

class HomeStateInitial extends HomeState {}

// ignore: must_be_immutable
class HomeStateLoaded extends HomeState {
  List<Feeds> feeds;
  List<User> friends;
  List<Story> stories;

  @override
  List<Object> get props => [feeds, friends, stories];
}

// ignore: must_be_immutable
class HomeStateSuccess extends HomeState {
  User user;
  bool isRefresh = false;
  HomeStateSuccess({this.user, this.isRefresh = false});
  @override
  List<Object> get props => [user, isRefresh];
}

class HomeStateFailed extends HomeState {}
