import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tappo/blocs/home/home_event.dart';
import 'package:tappo/blocs/home/home_state.dart';
import 'package:tappo/repositories/user_repository.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final UserRepository _userRepository;
  HomeBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(HomeStateInitial());

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    try {
      if (event is HomeEventInitial) {
        yield HomeStateLoaded();
        final currentUser = await _userRepository.getUserStorage();
        if (currentUser != null) {
          yield HomeStateSuccess(user: currentUser);
        } else {
          yield HomeStateFailed();
        }
      } else if (event is HomeEventLoaded) {
        yield HomeStateLoaded();
        final currentUser = _userRepository.currentUser;
        if (currentUser != null) {
          yield HomeStateSuccess(user: currentUser);
        } else {
          yield HomeStateFailed();
        }
      } else if (event is HomeEventRefresh) {
        yield HomeStateLoaded();
        final currentUser = _userRepository.currentUser;
        if (currentUser != null) {
          yield HomeStateSuccess(user: currentUser, isRefresh: true);
        } else {
          yield HomeStateFailed();
        }
      }
    } catch (e) {
      yield HomeStateFailed();
    }
  }
}
