import 'dart:async';
import 'dart:developer' as developer;
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:tappo/models/user.dart';

@immutable
abstract class SettingEvent extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SettingEventInitial extends SettingEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SettingEventLoading extends SettingEvent {}

class SettingEventPressSave extends SettingEvent {
  final User user;

  SettingEventPressSave({this.user});
  List<Object> get props => [user];
}

class SettingEventChangeAvatar extends SettingEvent {
  final File image;

  SettingEventChangeAvatar({this.image});
  @override
  // TODO: implement props
  List<Object> get props => [image];
}
