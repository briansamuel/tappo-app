import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';

import 'package:tappo/blocs/settings/setting_event.dart';
import 'package:tappo/blocs/settings/setting_state.dart';
import 'package:tappo/repositories/user_repository.dart';

class SettingBloc extends Bloc<SettingEvent, SettingState> {
  final UserRepository _userRepository;

  SettingBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(SettingStateInitial());

  @override
  Stream<SettingState> mapEventToState(SettingEvent settingEvent) async* {
    try {
      if (settingEvent is SettingEventInitial) {
        final _currentUser = _userRepository.currentUser;
        if (_currentUser != null) {
          yield SettingStateSuccess(user: _currentUser, isInit: true);
        } else {
          yield SettingStateFailed();
        }
      }
    } catch (e) {}
  }
}
