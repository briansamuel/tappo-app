import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:tappo/models/user.dart';

abstract class SettingState extends Equatable {
  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class SettingStateInitial extends SettingState {
  User user;
  // ignore: avoid_init_to_null
  SettingStateInitial({this.user});
  @override
  List<Object> get props => [user];
}

class SettingStateLoading extends SettingState {}

// ignore: must_be_immutable
class SettingStateSuccess extends SettingState {
  User user;
  // ignore: avoid_init_to_null
  File image = null;
  bool isInit;
  SettingStateSuccess({this.user, this.image, this.isInit = false});
  @override
  List<Object> get props => [user, image];
}

class SettingStateFailed extends SettingState {}
